<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
		<title>登录 </title>
		<link rel="stylesheet" href="css/reset.css" />
		<link rel="stylesheet" href="css/common.css" />

	</head>
	
	<body>
		<div class="wrap login_wrap">
			<div class="content">
				
				<div class="logo"></div>
				
				<div class="login_box">	
					
					<div class="login_form">
						<div class="login_title">
							登录
						</div>
						<form action="" method="post">
							
							<div class="form_text_ipt">
								<input name="username"  id="username" type="text" placeholder="手机号/邮箱">
							</div>
							<div class="ececk_warning"><span>数据不能为空</span></div>
							<div class="form_text_ipt">
								<input name="password" id="password" type="password" placeholder="密码">
							</div>
							<div class="ececk_warning"><span>数据不能为空</span></div>
							<div class="form_check_ipt">
								<%--<div class="left check_left">
									<label><input name="" type="checkbox"> 下次自动登录</label>
								</div>--%>
								<div class="right check_right" >
								<%--	<a  >忘记密码</a>--%>
								</div>
							</div>
							<div class="form_btn">
								<button type="button">登录</button>
							</div>
							<div class="form_reg_btn">
								<span>还没有帐号？</span><a href="register.jsp">马上注册</a>
							</div>
						</form>
						<div class="other_login">
							<%--<div class="left other_left">
								<span>其它登录方式</span>
							</div>
							<div class="right other_right">
								<a href="#">QQ登录</a>
								<a href="#">微信登录</a>
								<a href="#">微博登录</a>
							</div>--%>
						</div>
					</div>
				</div>
			</div>
		</div>



		<script type="text/javascript" src="js/jquery.min.js" ></script>
		<script type="text/javascript" src="js/common.js" ></script>
	<style>
.copyrights{text-indent:-9999px;height:0;line-height:0;font-size:0;overflow:hidden;}
</style>



<!-- 	判断验证格式 -->
		<script type="text/javascript">
		 var reg_tel = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;    //11位手机号码正则
		 var reg_password= /^(?![^a-zA-Z]+$)(?!\D+$)[a-zA-Z0-9!@#$%]{8,15}$/;  //密码正则
		$(document).ready(function () {
			$("button").click(function () {
			/* 	alert("值为："+$("#username").val()); */
				/* 	alert("值为："+$("#password").val()); */
			if($("#username").val()==""){
				alert("手机号码不能为空");
			}
			
			else	if($("#password").val()==""){
				alert("密码不能为空");
			}
			
			else{
				//alert("准备注册！");
				 //  var name=editor.txt.html();
				      // var role={"name":name,"film":film};            //新建一个json对象
				      
				      
				        var url="login.do";
				  $.post(
				           url,
				           
				        /*    {"data":JSON.stringify(name)}, */
				        {
				        	 phonenumber:$("#username").val(),
							password:$("#password").val()
				        	//data:"test测试"
				        },
				            function(data,status) {

				        	 // alert(data+"  状态:"+status);
				        	  $("#username").val("");
					             $("#password").val(""); 
					             $("#password2").val(""); 
				             
				             //  document.getElementById("te1").innerHTML=data;
				             
				        <%--    <%  response.setHeader("refresh", "6;url=index.jsp");%> --%>
				       if(data=="登录成功！"){
				        window.location.assign("index.jsp")//window.location 对象用于获得当前页面的地址 (URL)，并把浏览器重定向到新的页面。
				       }
				       else if(data=="登录失败！"){
									alert("账号或密码错误！")
								}
				       else{
				       	alert(data)
					   }
				        
				        }
				        );  
			        
				        
				        
				        
			}
			
			
			
			});
		});



		</script>


</body>
</html>