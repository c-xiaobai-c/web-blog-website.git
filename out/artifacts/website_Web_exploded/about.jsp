<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.dao.entiy.User"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>关于我</title>
<meta name="keywords" content="个人博客,WEB学习,WEB学习博客,XXX个人博客" />
<meta name="description" content="个人博客,WEB学习,WEB学习博客,XXX个人博客" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/base.css" rel="stylesheet">
<link href="css/about.css" rel="stylesheet"> 
 <link href="css/m.css" rel="stylesheet"> 
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<script src="js/jquery.min.js" ></script>
 <script src="js/ab.js"></script>
<link rel="stylesheet" type="text/css" href="css/wangEditor.css"><!-- 用于鼠标悬浮显示下拉列表 --> 

</head>
<body>

<%
         List<User>  user=(ArrayList)session.getAttribute("user");
        // String newsNumber= (String)session.getAttribute("news");
    List<String> newsNumber= (List<String>) session.getAttribute("news");
	%>

<header class="header-navigation" id="header">
  <nav>
    <div class="logo"><a href="http://wanwan.group/picture/a.jpg">小屋知多少</a></div>
    <h2 id="mnavh"><span class="navicon"></span></h2>
    <ul id="starlist">
      <li><a href="index.jsp">首页</a></li>
    <li><a href="upload.do">上传资源</a></li>
        <%if(newsNumber!=null&&!newsNumber.isEmpty()){if(!newsNumber.get(4).equals("0")){%>
        <li ><a href="news.do?" style="color: #e3514d">消息(<%=newsNumber.get(4)%>)</a>
        </li>
        <% }else{%>
        <li ><a href="news.do?" >消息</a>
        </li>
        <%}}else{%>
        <li ><a href="news.do?" >消息</a>
        </li>
        <%}%>
      <li><a  href="user/wangEditor.jsp">创作中心</a></li>
			
			<li><a href="about.jsp">关于我</a></li>
			
        <%
			if(user!=null&&!user.isEmpty()){
				for(User i : user){	%>	
      <li>     
 <div class="dropdown">
<img style=" border:none;border-radius:30px;box-shadow: 0px 0px 5px #ccc;  " alt="Paris" src="<%=i.getPicture()  %>"  width="50px" height="50px">
  <div class="dropdown-content">
      <a href="ArticleManager.do">博文管理</a>
    <a href="personalblog.do?name=<%=i.getName()%>">我的博客</a>
      <a href="editcenter.do?name=<%=i.getName()%>">个人中心</a>
      <a  href="quit.do?">退出</a>
  </div>
</div>
      <%}} else {  %>
          <li><a href="login.jsp">未登录 </a><%} %>
      
        </li>	
    </ul>
     <div class="searchbox">
    <div id="search_bar" class="search_bar">
      <form  id="searchform" action="searcharticle.do?" method="get" name="searchform" >
        <input class="input" placeholder="想搜点什么呢.." type="text" name="keyboard" id="keyboard">
        <input type="hidden" name="show" value="title" />
        <input type="hidden" name="tempid" value="1" />
        <input type="hidden" name="tbname" value="news">
        <input type="hidden" name="Submit" value="搜索" />
        <p class="search_ico"> <span></span></p>
      </form>
     </div> 
    </div>
     
  </nav>
</header>
<article>

   <div class="photowall">
    </ul>
  </div> 
  <div class="abox">
  <h2>站台简介</h2>
  <div class="biji-content" id="content">1.碌碌无为并非平凡可贵
2.今天不学习，明天变垃圾
3.努力只能及格，拼命才能优秀
4.你要悄悄拔尖，然后惊艳所有人
5.就这样头也不回的朝着光走下去吧

吧
6.你一定要努力，并且要和最好的人相遇
7.现在荒废的每一个瞬间，都是你的未来
8.总是有人要赢的，那为什么不能是我呢？
9.你总是这样轻言放弃的话，无论走多久都只会原地踏步
10.你可以笨，但你不可以懒！最可怕的不是别人放弃了你，而是你放弃了你自己！

11.没人喜欢学习可这社会只要成绩
12.不该有的感情要收起来 好好生活好好爱自己.
13.别在最好的年纪，辜负最好的自己.
14.努力一点吧 为了能走更好的路 遇见更优秀的人.
15.望自己成熟稳重善良填满优秀

16.会有始料不及的运气会有突如其来的惊喜.
17.宇宙山河浪漫，生活点滴温暖，都值得我前进
18.生活百般滋味 你要笑着面对.
19.努力过得充实 别留给自己难过的机会.
20.乾坤未定你我皆是黑马.

21.你必须拼尽全力 才有资格说自己运气不好。
22.生活再平凡 也是限量版 别轻看自己.
23.只要你肯去努力 你就是幸运的.
24.你一定要努力与更好的人相遇，
25.一定要优秀 堵住那悠悠众口。
26.少点以为，多点作为.
27.这个世界从来不公平，你只有努力，才能换来那些平等的待遇。
28.心有猛虎 细嗅蔷薇
29.你要优秀到所有人都不值得你委屈
30.觉得已经晚了的时候 就是最早的时候

31.若结局非你所愿.那就在尘埃落定前奋力一搏
32.总有不期而遇的温柔和生生不息的期待
33.希望日子和我都不会难过.干净脱俗温柔上进
34.接受自己的普通.然后拼尽全力去与众不同
35.别把最好的时光浪费在无所谓的等待.和犹豫不决
36.若结局非你所愿.那就在尘埃落定前奋力一搏.</div>
      </div>
   <div class="navlist">
    <ul>
      <li class="navcurrent"><a href="#top1">联系我们</a> </li>
    </ul>
  </div>
  
   <!--  <div class="navitem" style="display: block;" name = "top1">
      <div class="content"> -->
      <!--   <p>反馈信息：<input></p>
        <p>职业：JavaWeb开发工程师、网页设计 </p>
                <p>邮箱：18210493942@163.com</p> -->
      <!--   <p>公众号：</p>
        <p><img src="images/wxgzh.jpg"></p> -->
         <div class="abox">
​

<!-- 您的邮箱: <input type="text" name="FirstName" value="" style="width: 100%;height:30px;border: none ; outline: none;background:#DCDCDC; "><br> -->
内容填写: <!-- <input type="text" name="LastName" value="Mouse"><br> -->
 <br><textarea id="mailmessage" name="mailmessage" placeholder="请输入你的建议" style="width: 100%;height:100px;border: none ; outline: none;background:#DCDCDC; "></textarea><br>
<input type="submit" id="sendmail" value="反馈我们" style="width: 100%;height:100px;border: none ; outline: none;background:#ffffff; ">

       
     
  
   </div> 
  
  
    
  
  
 
 
</article>
<footer>
  <p>Design by <a href="http://wanwan.group" target="_blank">c小白c| &nbsp 备案号：</a> <a href="https://beian.miit.gov.cn/#/Integrated/recordQuery" target="_blank">粤ICP备2021050792号</a></p>
</footer>
<a href="#" class="cd-top">Top</a> 
<script type="text/javascript">
$("#sendmail").click(function(){
	$("#sendmail").hide();
	$("#mailmessage").after('<img id="load" src="images/load.gif"/>');
$.post("sendmail.do",
	    {
	    // mailmessage:document.getElementById("mailmessage").value
	 mailmessage:$("#mailmessage").val()
	    },
	    function(data,status){
	       // alert("数据: \n" + data + "\n状态: " + status);
	        if(data=="false"){
	        	alert("发送失败");
	        }
          if(data=="sucessful"){
        	  alert("发送成功");
	        }
          if(data=="exist"){
        	  alert("已发送，请勿重复！");
 	
           }
          $("#sendmail").show();
          $("#load").remove();	
	    });

});

</script>
</body> 
</html>

