<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/5/28
  Time: 15:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.dao.entiy.User"%>
<%@ page import="com.dao.entiy.Article" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="http://wan.wan.group/picture/a.jpg" type="image/x-icon"><!-- 在网页标题左侧显示图标 -->
    <link rel="shortcut icon" href="http://wan.wan.group/picture/a.jpg" type="image/x-icon"><!-- 在收藏夹显示图标 -->
    <meta name="keywords" content="个人博客,WEB学习,WEB学习博客,XXX个人博客" />
    <meta name="description" content="个人博客,WEB学习,WEB学习博客,XXX个人博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/about.css" rel="stylesheet">
    <link href="../css/m.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <script src="../js/jquery.min.js" ></script>
    <script src="../js/PersonArticle.js"></script>




    <link href="../css/index.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/wangEditor.css"><!-- 用于鼠标悬浮显示下拉列表  -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.css"><!-- 点赞，收藏图标  -->
    <script src="../js/scrollReveal.js"></script>
    <link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.css"><!-- 方向图标，加载图标  -->

    <base target="_parent" />

    <title>文章管理</title>
</head>
<body>
<%
    List<User>  user=(ArrayList)session.getAttribute("user");
    List<String> newsNumber= (List<String>) session.getAttribute("news");
    List<Article>  articles=(ArrayList)request.getAttribute("articleManager");
    if(user!=null&&!user.isEmpty()){
        User user2=user.get(0);

    }
    else {
        response.sendRedirect("../login.jsp");

    }
%>





<header class="header-navigation" id="header">
    <nav>
        <div class="logo"><a href="http://121.196.147.151/a.jpg">小屋知多少</a></div>
        <h2 id="mnavh"><span class="navicon"></span></h2>
        <ul id="starlist">
            <li><a href="../index.jsp">首页</a></li>
            <li><a href="../upload.do">上传资源</a></li>
            <%if(user!=null&&!user.isEmpty()){if(!newsNumber.get(4).equals("0")){%>
            <li ><a href="../news.do?" style="color: #e3514d">消息(<%=newsNumber.get(4)%>)</a>
            </li>
            <% }else{%>
            <li ><a href="../news.do?" >消息</a>
            </li>
            <%}}else{%>
            <li ><a href="../news.do?" >消息</a>
            </li>
            <%}%>
            <li><a  href="user/wangEditor.jsp">创作中心</a></li>

            <li><a href="../about.jsp">关于我</a></li>

            <%
                if(user!=null&&!user.isEmpty()){
                    for(User i : user){	%>
            <li>
                <div class="dropdown">
                    <img style=" border:none;border-radius:30px;box-shadow: 0px 0px 5px #ccc;  " alt="Paris" src="<%=i.getPicture()  %>"  width="50px" height="50px">
                    <div class="dropdown-content" style=":position:absoulte;z-index:5555;">
                        <a href="ArticleManager.do">博文管理</a>
                        <a href="personalblog.do?name=<%=i.getName()%>">我的博客</a>
                        <a href="editcenter.do?name=<%=i.getName()%>">个人中心</a>
                        <a  href="quit.do?">退出</a>
                    </div>
                </div>
                    <%}} else {  %>
            <li><a href="login.jsp">未登录 </a><%} %>

            </li>
        </ul>
        <div class="searchbox">
            <div id="search_bar" class="search_bar">
                <form  id="searchform" action="searcharticle.do?" method="get" name="searchform" >
                    <input class="input" placeholder="想搜点什么呢.." type="text" name="keyboard" id="keyboard">
                    <input type="hidden" name="show" value="title" />
                    <input type="hidden" name="tempid" value="1" />
                    <input type="hidden" name="tbname" value="news">
                    <input type="hidden" name="Submit" value="搜索" />
                    <p class="search_ico"> <span></span></p>
                </form>
            </div>
        </div>

    </nav>
</header>

<%--

<div style="padding-top: 70px;"> <iframe id="zi" frameborder="0" src="user/editcenter2.jsp" width="100%"  height="700px" ></iframe></div>

--%>

<article >

    <div class="blogs" align="center" style="margin-top: 0px;" >  <span style="color: #253fff" onclick="showarticle()" id="article1">文章管理</span><span>&nbsp;&nbsp;&nbsp;&nbsp;</span><span id="article2" onclick="historyArticle()">历史浏览</span></div>
    <%--
        <div class="blogs"  style="float: left;margin-right: 10px" ><p>xxxxxxxx</p><p>xxxxxxxx</p><p>xxxxxxxx</p><p>xxxxxxxx</p></div>
    --%>
    <div id="newArticle">
        <%
            if(articles!=null&&!articles.isEmpty()){
                for (Article i : articles) { %>
        <div class="blogs" data-scroll-reveal="enter bottom "  id=<%=i.getId()%>>
            <a target="_blank"
               href="articleEditor.do?cam223c=1&cnmnam=5&cbfbahf=5&caxsx=5&articleid=<%=i.getId()%>&name=<%=i.getName()%>">
                <h3 class="blogtitle"><%=i.getTitle()    %>
                </h3>   </a>
            <div style=" max-height: 150px;overflow: hidden;text-overflow: ellipsis;hite-space: nowrap;">
                <%=i.getContent()  %>
            </div>
            <div class="bloginfo">
                <ul>
                    <%--   <li class="author"><%=i.getName()%></li>
                       <li class="lmname"><%=i.getArticlelabel()%></li>--%>
                    <input type="hidden" value="<%=i.getName()%>" id="name">
                    <li class="timer"><%=i.getTime()%>
                    </li>
                    <li class="view"><span><%=i.getPageview()%></span>已阅读</li>
                    <li class="like"><%=i.getLikenumber()%>
                    </li>
                    <li><i class="fa fa-commenting-o" style="font-size:14px"></i> <%=i.getComment()%>
                    </li>
                    <li><i class="fa fa-star" style="font-size:14px"></i><%=i.getCollect()%>
                    </li>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li><h3 style="color: #e7ae71"><%=i.getStatus()%></h3></li>
                        <li></li>
                        <li><h3 style="color: #e7ae71"><%=i.getModality()%></h3></li>
                        <li>  <a target="_blank" href="articleEditor.do?cam223c=1&cnmnam=5&cbfbahf=5&caxsx=5&articleid=<%=i.getId()%>&name=<%=i.getName()%>"><h3 style="color: #253fff">编辑</h3> </a></li>
                    <li>  <a target="_blank" href="personarticle.do?cam223c=1&cnmnam=5&cbfbahf=5&caxsx=5&articleid=<%=i.getId()%>&name=<%=i.getName()%>"><h3 style="color: #253fff">浏览</h3></a></li>
                    <li><h3 style="color: #253fff" onclick=deleteArticle("<%=i.getId()%>","<%=i.getName()%>")>删除</h3></li>
                </ul>
            </div>

        </div>


        <%}}%>
    </div>
    <div id="historyarticle" style="display: none;"> <h3 style="color: #253fff" onclick="deletehistory()">清除浏览历史</br> </h3></div>
    <style>
        div.center {text-align: center;}
    </style>
    <div  class="center" onclick="updateArticle()" id="update">  <i  style="font-size:54px;" class="fa">&#xf103</i></div>
    <div  class="center" style="display: none" onclick="historyArticle()" id="update2">  <i  style="font-size:54px;" class="fa">&#xf103</i></div>

</article>

<script >
    var n=1;
    var n2=0;
    var num;
    function updateArticle() {


        var url="ArticleManager.do";
        n++;
        num=n;

        $("#update").hide();
        $("#update2").hide();
        $("#update").after('<img id="load" src="../images/load.gif"/>');
        $.post(
            url,

            {
                n:num  //页数

            },

            function(data) {

                $("#update").show();
                if(data=="失败了！"){
                    alert("没有数据了")
                    $("#load").remove();
                }
                else{
                    for (var l = 0; l < 5; l++) {
                        $("#load").remove();
                    }
                    let json = JSON.parse(data);
                    // for (i in json)
                    for (var i = 0; i < 5; i++) {
                        var txt0 = "href=\"articleEditor.do?cam223c=1&cnmnam=5&cbfbahf=5&caxsx=5&articleid=" + json[i].id + "&name=" + json[i].name + "\"";
                        var txtt= "<a  target=_blank " + txt0 + ">";
                        var txt = "href=\"personarticle.do?cam223c=1&cnmnam=5&cbfbahf=5&caxsx=5&articleid=" + json[i].id + "&name=" + json[i].name + "\"";
                        var txt1 = "<a  target=_blank " + txt + ">";
                        var txt2 = " <div   id='"+json[i].id+"' class=\"blogs\"  data-scroll-reveal=\"enter bottom over 1s\"  >";              // 使用 HTML 标签创建文本
                        var txt3 = " <h3 class=blogtitle >" + json[i].title + "</h3> </a>";  // 使用 jQuery 创建文本
                        var txt4 = "<div   style=max-height:150px;overflow:hidden;text-overflow:ellipsis;hite-space:nowrap; >" + json[i].content + " </div>";
                        // var txt4=" <div class=bloginfo> <ul> <li class=author>"+json[i].name+"</li> <li class=lmname >"+json[i].articlelabel+"</li> <li class=timer>"+json[i].time+"</li> <li class=view><span>"+json[i].pageview+"</span>已阅读</li> <li class=like>"+json[i].likenumber+"</li> </ul>  </div> </div> ";
                        var txt5 = " <div class=bloginfo> <ul>  <li class=timer>" + json[i].time + "</li> <li class=view><span>" + json[i].pageview + "</span>已阅读</li> <li class=like>" + json[i].likenumber + "</li>";
                        var txt6 = " <li>  <i  style=font-size:14px  class=\"fa fa-commenting-o\"></i> " + json[i].comment + "</li> <li> <i class=\"fa fa-star\" style=font-size:14px></i>" + json[i].collect + "</li>       <li>   <li><h3 style=\"color: #e7ae71\">"+json[i].status+"</h3></li>\n" +
                            "                        <li></li><li><h3 style=\"color: #e7ae71\">"+json[i].modality+"</h3></li>&nbsp;&nbsp;&nbsp;&nbsp;</li>\n" +
                            "                        <li>"+txtt+"<h3 style=\"color: #253fff\">编辑</h3></a> </li>\n" +
                            "                        <li>"+txt1+"<h3 style=\"color: #253fff\">浏览</h3></a></li>\n" +
                            "                        <li><h3 style=\"color: #253fff\" onclick=deleteArticle('"+json[i].id+"','"+json[i].name+"')>删除</h3></li></ul></div></div>"

                        $("#newArticle").append(txt2 + txtt + txt3 + txt4 + txt5 + txt6);        // 追加新元素


                    }
                }

            }

        );

    }

    function historyArticle() {

        var url="historyArticle.do";
        n2++;
        num=n2;
        $("#update").hide();
        $("#update2").hide();
        $("#newArticle").hide();
        $("#historyarticle").show();
        $("#article2").css("color", "#253fff");
        $("#article1").css("color", "#1b1c1d");

        $("#update2").after('<img id="load" src="../images/load.gif"/>');
        $.post(
            url,

            {
                n:num  //页数

            },

            function(data) {

                $("#update2").show();
                if(data=="失败了！"){
                    alert("没有数据了")
                    $("#load").remove();
                }
                else{
                    for (var l = 0; l < 5; l++) {
                        $("#load").remove();
                    }
                    let json = JSON.parse(data);
                    // for (i in json)
                    for (var i = 0; i < 5; i++) {
                        var txt = "href=\"personarticle.do?cam223c=1&cnmnam=5&cbfbahf=5&caxsx=5&articleid=" + json[i].id + "&name=" + json[i].name + "\"";
                        var txt1 = "<a  target=_blank " + txt + ">";
                        var txt2 = " <div id=article class=\"blogs\"  data-scroll-reveal=\"enter bottom over 1s\"  >";              // 使用 HTML 标签创建文本
                        var txt3 = " <h3 class=blogtitle >" + json[i].title + "</h3> </a>";  // 使用 jQuery 创建文本
                        var txt4 = "<div   style=max-height:150px;overflow:hidden;text-overflow:ellipsis;hite-space:nowrap; >" + json[i].content + " </div>";
                        // var txt4=" <div class=bloginfo> <ul> <li class=author>"+json[i].name+"</li> <li class=lmname >"+json[i].articlelabel+"</li> <li class=timer>"+json[i].time+"</li> <li class=view><span>"+json[i].pageview+"</span>已阅读</li> <li class=like>"+json[i].likenumber+"</li> </ul>  </div> </div> ";
                        var txt5 = " <div class=bloginfo> <ul>  <li class=timer>" + json[i].time + "</li> <li class=view><span>" + json[i].pageview + "</span>已阅读</li> <li class=like>" + json[i].likenumber + "</li>";
                        var txt6 = " <li>  <i  style=font-size:14px  class=\"fa fa-commenting-o\"></i> " + json[i].comment + "</li> <li> <i class=\"fa fa-star\" style=font-size:14px></i>" + json[i].collect + "</li>       <li>&nbsp;&nbsp;&nbsp;&nbsp;</li>\n" +
                            "                        <li><h3 style=\"color: #253fff\">浏览时间：" +json[i].historytime+ "</h3> </li></ul></div></div>"

                        $("#historyarticle").append(txt2 + txt1 + txt3 + txt4 + txt5 + txt6);        // 追加新元素


                    }
                }

            }

        );

    }

    function showarticle() {
        $("#newArticle").show();
        $("#update2").hide();
        $("#update").show();
        $("#historyarticle").hide();
        $("#article1").css("color", "#253fff");
        $("#article2").css("color", "#1b1c1d");

    }
    function deleteArticle(articleid,name) {
        // alert("articleid"+articleid+"name"+name);
        var tf = confirm("确定要删除吗？");
        if (tf) {
            $.post("deleteArticle.do",
                {
                    name: name,
                    articleid: articleid
                },
                function (data) {
                    if (data == "删除成功") {
                        alert(data);
                        $("#" + articleid).remove();
                    } else {
                        alert(data)
                    }

                });

        }
    }
    function deletehistory() {
        var tf = confirm("确定要删除历史吗？");
        if (tf) {
            $.post("deletehistory.do",
                {
                },
                function (data) {
                    if (data == "删除成功") {
                        alert(data);

                    } else {
                        alert(data)
                    }

                });

        }
    }

</script>
</body>
</html>