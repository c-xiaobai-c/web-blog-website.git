<%@page import="javax.servlet.jsp.tagext.TryCatchFinally"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@page import="java.util.ArrayList"%>
 <%@page import="java.util.List"%>
<%@page import="com.dao.entiy.User"%>
<%@ page import="com.dao.entiy.Article" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<title>写文章</title>
	<link rel="icon" href="http://wanwan.group/picture/a.jpg" type="image/x-icon"><!-- 在网页标题左侧显示图标 -->
	<link rel="shortcut icon" href="http://wanwan.group/picture/a.jpg" type="image/x-icon"><!-- 在收藏夹显示图标 -->
	<link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script> 	
	<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- <script src="https://cdn.jsdelivr.net/npm/wangeditor@latest/dist/wangEditor.min.js"></script>  -->
	<script src="../js/wangEditor.min.js"></script> 	
	<script src="http://cdn.bootcss.com/highlight.js/8.0/highlight.min.js"></script>
   <link href="http://cdn.bootcss.com/highlight.js/8.0/styles/monokai_sublime.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../css/wangEditor.css"><!-- 用于鼠标悬浮显示下拉列表 --> 
<script src="../js/safe.js"></script>
</head>
<body >


<%
try {
	List<User>   user=(ArrayList)session.getAttribute("user");//获取保存在会话中的用户账号
	List<Article>  list=(ArrayList<Article>)request.getAttribute("updateArticle");
	if(user!=null&&!user.isEmpty()){
		User user2=user.get(0); 
%>
		    

	      
	
  
<!-- <form class="form-inline" role="form"> -->

<div class="page-header"  >
    <h1 style="width:100%; "> <a href="../index.jsp"   style="width:8%;">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
    
    
        <small style="width:8%;"> 文章管理</small>
		<%
			if(list!=null&&!list.isEmpty()){
				for(Article i : list){
		%>
		<small> <input  style="width:60%;" style=" width:60%; height:50px; line-hight:50px; background-color:#eaf1f0;text-align:center;
        border-radius: 20px; outline: none;" type="text"  id="title"  value="<%=i.getTitle()%>"</small>

		<%}}else{%>
		<small><input   style="width:60%;" style=" width:60%; height:50px; line-hight:50px; background-color:#eaf1f0;text-align:center;
        border-radius: 20px; outline: none;" type="text"  id="title"  placeholder="请输入标题" ></small>

		<%}%>
       <small style="width:8%;"><button  style="width:8%;" class="btn btn-primary btn-lg">存为草稿</button> </small>
     <small style="width:8%;"><button  style="width:8%;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">发表文章
</button> </small>
   
      <small  style="width:8%;">

  
 <div class="dropdown">
<!--    <img  src="http://121.196.147.151/a.jpg" class="img-circle" width="50px" height="50px"> -->
<img  src="<%=user2.getPicture()  %>" class="img-circle" width="50px" height="50px"> 
 <input  id="name" style="display: none" value="<%= user2.getName() %>" type="button">
 <input  id="phonenumber" style="display: none" value="<%=user2.getPhonenumber()  %>" type="button">
  <div class="dropdown-content">
	  <a href="../ArticleManager.do">博文管理</a>
	  <a href="../personalblog.do?name=<%=user2.getName()%>">我的博客</a>
	  <a href="../editcenter.do?name=<%=user2.getName()%>">个人中心</a>
	  <a  href="../quit.do?">退出</a>
  </div>
</div>
     </small>
 
    </h1>
   
</div>
	

<!-- </form> -->
<%
	if(list!=null&&!list.isEmpty()){
		for(Article i : list){
%>
<input type="hidden" id="edit" value="更新文章">
<input type="hidden"  id="editArticleid" value="<%=i.getId()%>">
<div id="div1"  style="padding-top:10px; " >
	<%=i.getContent() %>
</div>
<% } }else{%>
<input type="hidden" id="edit" value="新文章">
<input type="hidden"  id="editArticleid" value="0">
<div id="div1"  style="padding-top:10px; " >
	<p>欢迎使用 ，<b></b>快来创作吧</p>
</div>

<%}%>
<!-- 模态框（Modal） -->
<div   style="padding-top:10%; "class="modal fade"  data-backdrop="static" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="myModalLabel">
					发布文章
				</h4>
			</div>
			<div class="modal-body" >
			<form> 
			<div  style="padding-top:20px;">
			<label style="padding-right: 20px;"> 文章标签</label><input  id="text1"  type="text">
					<select  id="articleLabel">
		 <option value="添加文章标签" selected>添加文章标签</option>
        <option value="编程" >编程</option>
        <option value="生活">生活</option>
        <option value="新闻">新闻</option>
        </select>
			<br>
			</div>
			<div  style="padding-top: 20px;">
			<label  style="padding-right: 20px;"> 分栏专类</label><input id="subfield" type="text">
			<br>
			</div>
			<div  style="padding-top:20px; z-index:99999999;">
			<label  style="padding-right: 20px;"> 文章类型</label>
			<select id="selects">
        <option value="原创" selected>原创</option>
        <option value="转载">转载</option>
        <option value="翻译">翻译</option>
</select>
	
		</div>	
		
		
			<br>
			
				<label  style="padding-right: 20px;"> 发布形式</label>
           <input type="radio" name="type" value="公开"  checked="checked"/>公开
            <input type="radio" name="type" value="私密"/>私密
        
            <br />  
         
		
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" >关闭
				</button>
				<button type="button" class="btn btn-primary"   data-dismiss="modal" id="send">
					提交更改
				</button>
			</div>
		</div>
	</div>
</div> 

 


<!-- 富文本编辑器wangEditor的使用： -->
<script type="text/javascript">

    const E = window.wangEditor
    const editor = new E('#div1')
editor.config.height = 700//设置高度
editor.config.zIndex = 1000//编辑器 z-index 默认为 10000，可以自行调整。
editor.config.focus = false//编辑器初始化时，默认会自动 focus 到编辑区域。可通过如下操作，取消自动 focus 。
// 当我们使用兼容模式的时候，可以自定义记录的频率，默认 200 ms
editor.config.onchangeTimeout = 500 // 修改为 500 ms
// 还可以修改历史记录的步数。默认 30 步
editor.config.historyMaxSize = 50 // 修改为 50 步
editor.highlight = hljs     //代码高亮
// 菜单栏提示为下标
editor.config.menuTooltipPosition = 'down'
//默认限制图片大小是 5M ，可以自己修改。
editor.config.uploadImgMaxSize = 2 * 1024 * 1024 // 2M
editor.config.uploadImgMaxLength =2 // 一次最多上传 5 个图片
editor.config.uploadImgShowBase64 = true
editor.config.uploadVideoServer = '/video'

   editor.create()

 
	
    
</script>
<!-- 发布文章 -->
     <script>  
    var text2;
    var value2;
    $("#articleLabel").change(function(){ //下拉框改变时取得值
     
        var obj = document.getElementById("articleLabel"); //获取下列表中被选中的值
        var index = obj.selectedIndex; // 选中索引
      text2 = obj.options[index].text; // 选中文本
      value2 = obj.options[index].value; // 选中值
        document.getElementById("text1").value= value2;
      //alert(text2);
});
    

            function publish(){ //发布文章
                var radioValue = $("input[name='type']:checked").val();  // //获取单选按钮的值
                
                
             
                
                var obj = document.getElementById("selects");//获取下列表中被选中的值(文章类型)
                var index = obj.selectedIndex; // 选中索引
                var text = obj.options[index].text; // 选中文本
                var value = obj.options[index].value; // 选中值
            
          //   alert(radioValue);
                
     
            var url="savearticle.do"; 
			  $.post(
			           url,
			      
			        {
			        	 name:$("#name").val() ,  //昵称
			        	 phonenumber:$("#phonenumber").val() ,//手机号
			        	 content:editor.txt.html(),   //文章内容
			        	 title:$("#title").val() ,   //文章标题
			        	 articlelabel:$("#text1").val(),//文章标签
			        	 subfield:$("#subfield").val(),//分栏专类
			        	 articletype:value,//文章类型
						 check:$("#edit").val(),
						articleid:$("#editArticleid").val(),
			        	 modality:radioValue
			        
			        	
			        },
			            function(data,status) {
			        	//  alert(data+"  状态:"+status);
			       
			       if(data=="发布成功！"){
			    	   alert("发布成功！");
			      window.location.assign("../index.jsp")//window.location 对象用于获得当前页面的地址 (URL)，并把浏览器重定向到新的页面。
			 
			       }
			       else if(data=="内容过多，上传失败！"){
			    	   alert("发布失败,图片过大");
			   
			 
			       }
			       else{
			    	   alert("发布失败，内容过大！");
			       }
			 /*    var json = JSON.parse(data);
			    var x;
			    for (i in json) {
			        x += json[i].content + "<br>";
			    }
			        	  $("#test1").html(x);  */
			        }
			        );  
		        
            
            }  
            </script>      
<!-- 使用模态框 进行数据交互 -->

<script type="text/javascript">
var name;
$('#myModal').on('hidden.bs.modal', function () {
	//window.location.assign("http://localhost:8080/Website/indite.jsp")
	 
//alert($("#demo").val()+name);
	})

	

	$("#send").click(function (){

		name=editor.txt.html();
	
    if(editor.txt.text()==""){
	
	alert("内容不能为空");
	
    }
    else if($("#title").val()==""){
    	alert("标题不能为空");
    }
    else {
    
    	publish();//调用发送函数
    	//alert($("#subfield").val() );
    }
   
	

		
	});

</script>

  <% 
	 }else {
	 response.sendRedirect("../login.jsp");
	
	 }
	 
} catch (Exception e) {
	// TODO: handle exception
	System.out.println(e);
	// response.sendRedirect("../login.jsp");
}

%>

 
</body>
</html>