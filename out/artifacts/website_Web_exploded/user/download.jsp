<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/5/22
  Time: 1:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@page import="com.dao.entiy.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="com.dao.entiy.UserDocument" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>下载列表</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<%
    List<User>  user=(ArrayList)session.getAttribute("user");
    //List<String> newsNumber= (List<String>) session.getAttribute("news");
    if(user!=null&&!user.isEmpty()){
        User user2=user.get(0);
        if (!user2.getLevel().equals("超级会员")) {//判断是否是超级用户
            response.sendRedirect("login.jsp");
        }

    }
    else {
        response.sendRedirect("login.jsp");

    }
    List<UserDocument>  list=(ArrayList)session.getAttribute("userdocument"); %>

<%--<div >    <iframe id="zi" frameborder="0" src="user/head.jsp" width="100%"  height="70px"  ></iframe></div>--%>

<table class="table table-striped">
    <!-- <caption>条纹表格布局</caption> -->
    <thead>
    <tr>
        <th>序列</th>
        <th>名称</th>
        <th>时间</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <%
        if(list!=null&&!list.isEmpty()){
            for(UserDocument userDocument : list){	%>
    <tr>
        <td><%=userDocument.getId()%></td>
        <td><%=userDocument.getFilename()%></td>
        <td><%=userDocument.getTime()%></td>
        <td onclick="name()" > <a href="download.do?filename=<%=userDocument.getAnotherfilename()%>">下载</a></td>
        <td onclick="name()" > <a href="delete.do?filename=<%=userDocument.getAnotherfilename()%>&id=<%=userDocument.getId()%>">删除</a></td>
    </tr>
    <%}} %>
    </tbody>
</table>

<script type="text/javascript">
    function name() {
        //alert("test");
    }

</script>
</body>
</html>