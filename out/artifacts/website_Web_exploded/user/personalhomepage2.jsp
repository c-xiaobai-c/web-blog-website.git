<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dao.entiy.User" %>
<%@ page import="com.dao.entiy.StatisticalList" %><%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/5/22
  Time: 21:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<html>
<head>
    <title>Title</title>
    <style> .table>tbody>tr>td{border:0px;
    }</style>
</head>
<body>
<%
    List<User>  user0=(ArrayList)session.getAttribute("user");//登录的用户信息
    List<User>  user=(ArrayList)session.getAttribute("person1");//博主信息
    StatisticalList statisticalLists= (StatisticalList) session.getAttribute("person2");//博主的文章数量，粉丝数量，被访问数量
    StatisticalList statisticalLists2= (StatisticalList) session.getAttribute("guanzhu");//博主的是否被登录的用户关注

   /* if(user!=null&&!user.isEmpty()){
        User user2=user.get(0);
        if (!user2.getLevel().equals("超级会员")) {//判断是否是超级用户
            response.sendRedirect("login.jsp");
        }
    }
    else {
        response.sendRedirect("login.jsp");

    }*/
%>
<% if(user!=null&&!user.isEmpty()){
    for(User i : user){
%>

<div class="table-responsive">
    <table class="table" >
        <tbody  class="text-center">
        <tr align="center" >
         <td></td>
            <td >
            <img style=" border:none;border-radius:80px;box-shadow: 0px 0px 5px #ccc;  " alt="Paris" src="..<%=i.getPicture()%>"  width="100px" height="100px">
            </td>
            <% if(user0!=null&&!user0.isEmpty()){//用于判断是他人个人主页还是用户的个人主页
                 User user1=user0.get(0);
                 if(user1.getName().equals(i.getName())){
            %>

            <td><br/><h4 id="editcenter">个人中心</h4></td>
           <%}else{ %>
                       <td><br/><br/><% if(statisticalLists2!=null){if(statisticalLists2.getFasstatus()!=null){%>
                           <h4 id="noguanzhu" >已关注</h4>
                           <h4 id="guanzhu" style="display: none">关注</h4>
                           <%}else{%>
                           <h4 id="guanzhu">关注</h4>
                           <h4 id="noguanzhu" style="display: none">已关注</h4></td>

          <% }}else{%>
            <h4 id="guanzhu">关注</h4>
            <h4 id="noguanzhu" style="display: none">已关注</h4></td>
                 <%}}}%>
       </tr>
        <tr> <td></td>
            <td id="name"><%=i.getName()%></td>
            <td></td>
        </tr>
        <% if(statisticalLists!=null){

        %>

        <tr   align="center">
            <td>被访问量<h3> <%=statisticalLists.getPageviewsum()%></h3></td>
            <td>原创文章 <h3><%=statisticalLists.getArticlesum()%></h3></td>
            <td>粉丝数量 <h3 > <%=statisticalLists.getFanssum()%></h3></td>
        </tr>
        <%}else{%>
        <tr   align="center">
            <td>被访问量<h3> 0</h3></td>
            <td>原创文章 <h3>0</h3></td>
            <td>粉丝数量 <h3> 0</h3></td>
        </tr>
<%}%>

        </tbody>
    </table>
</div>
<script>
    $(document).ready(function(){

        $("#guanzhu").click(function(){


            $.post("fans.do",
                {
                    name:$("#name").text()

                },
                function(data,status){
                   // alert("数据: \n" + data + "\n状态: " + status);
                    if(data=="成功！"){
                        $("#guanzhu").hide();
                        $("#noguanzhu").show();
                    }
                    else {
                        alert("关注失败")
                    }
                });
        });
        $("#noguanzhu").click(function(){

            $.post("nofans.do",
                {
                    name:$("#name").text()

                },
                function(data,status){
                    // alert("数据: \n" + data + "\n状态: " + status);
                    if(data=="成功！"){
                        $("#noguanzhu").hide();
                        $("#guanzhu").show();
                    }
                    else {
                        alert("取消关注失败")
                    }
                });
        });
    });
</script>



<% }}
else{%>

    <script>
        window.parent.location.assign("../user/error.html")//window.location 对象用于获得当前页面的地址 (URL)，并把浏览器重定向到新的页面。
    </script>
<%}
request.getSession().removeAttribute("person1");//清除session缓存，避免数据读取错误
request.getSession().removeAttribute("person2");%>

</body>
</html>
