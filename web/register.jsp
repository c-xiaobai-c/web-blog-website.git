<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
		<title>注册</title>
		<link rel="stylesheet" href="css/reset.css" />
		<link rel="stylesheet" href="css/common.css" />
		<script src="../js/jquery.min.js" ></script> 
		
	</head>
	
	<body >
	
		<div class="wrap login_wrap  " >
			<div class="content">
				
				<div class="logo"></div>
				
				<div class="login_box">	
					
					<div class="login_form">
						<div class="login_title">
							注册
						</div>
						<form action="" method="post">
							
							<div class="form_text_ipt">
								<input name="username" id="username" type="text" placeholder="手机号/邮箱">
							</div>
								<div class="ececk_warning"><span>数据不能为空</span></div>
						 <div class="form_text_ipt">
								<input name="username2" id="username2" type="text" placeholder="昵称(只能为中文)">
							</div>
							<div class="ececk_warning"><span>数据不能为空</span></div>
							<div class="form_text_ipt">
								<input name="password" type="password" id="password"  placeholder="密码(必须包括字母和数字,字符长度9--15之间)">
							</div>
							<div class="ececk_warning"><span>数据不能为空</span></div>
							<div class="form_text_ipt">
								<input name="repassword" type="password" id="password2"  placeholder="重复密码">
							</div>
							<div class="ececk_warning"><span>数据不能为空</span></div>
						<div class="form_text_ipt">
						<!-- 	 <button  id="send" onclick="send()" type="button">发送验证码</button> --> 
								 <input  id="send1" value="发送验证码" type="button"  onclick="send()"> 
							 <input  id="send2" style="display: none" value="发送验证码2" type="button">
							</div>
							<div class="form_text_ipt">
								<input  name="code" id="code" type="text" placeholder="验证码">
							</div>
							
							
							<div class="ececk_warning"><span>数据不能为空</span></div>
							
							<div class="form_btn">
								<button type="button">注册</button>
							</div>
							<div class="form_reg_btn">
								<span>已有帐号？</span><a href="login.jsp">马上登录</a>
							</div>
						</form>
					<!-- 	<div class="other_login">
							<div class="left other_left">
								<span>其它登录方式</span>
							</div>
							 <div class="right other_right">
								<a href="#">QQ登录</a>
								<a href="#">微信登录</a>
								<a href="#">微博登录</a>
							</div> 
						</div> -->
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="js/jquery.min.js" ></script>
		<script type="text/javascript" src="js/common.js" ></script>
		
	<!--
	验证码倒计时
	  -->	
	<script>
  
	function send() {
		
		 var reg_tel = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;    //11位手机号码正则
			if($("#username").val()==""){
				alert("手机号码不能为空");
			}
			else if(!reg_tel.test($("#username").val())) {
					alert("请正确填写您的手机号码！");
			
				} 
			else{
			     var url="code.do";
				  $.post(
				           url,
				 
				        {
				        	 phonenumber:$("#username").val(),
				       
				        },
				           function(data,status) {
				        	  alert(data+"  状态:"+status);
				        	
				        	  if(data=="发送成功"){
				        		 var i=120;
				        			var myVar=setInterval(function(){//定时器，每秒一次循环
				        				//	document.getElementById("send2").innerHTML = i;
				        					document.getElementById("send2").value = i;//给标签赋值
				        				  // i--;
				        				 name();
				        				  $("#send2").show();
				        				  $("#send1").hide();
				        				   if(i==-1){
				        				 		 $("#send2").hide();//隐藏标签
				        				 		 $("#send1").show();//显示标签		
				        						 
				        					}  
				        					},1000);	
				        				
				        			function name() {
				        					i--;
				        				 	if(i==-1){
				        				 		clearInterval(myVar);//关闭定时器
				        				 		// $("#send2").hide();
				        				 		// $("#send1").show();
				        						
				        						 
				        					} 
			        				
				        			
				        			}
				        	  }
				           });
				
			}
		
	
	}
</script>
	<!-- 	判断验证格式 -->
		<script type="text/javascript">
		 var reg_tel = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;    //11位手机号码正则
		 var reg_password= /^(?![^a-zA-Z]+$)(?!\D+$)[a-zA-Z0-9!@#$%]{8,15}$/;  //密码正则
		 var reg_name = /[\u4E00-\u9FA5\uF900-\uFA2D]/;//昵称正则
		$(document).ready(function () {
			$("button").click(function () {
			/* 	alert("值为："+$("#username").val()); */
				/* 	alert("值为："+$("#password").val()); */
			if($("#username").val()==""){
				alert("手机号码不能为空");
			}
			else if(!reg_tel.test($("#username").val())) {
					alert("请正确填写您的手机号码！");
			
				} 
			else if($("#username2").val()==""){
				alert("昵称不能为空");
			}	
			else if(!reg_name.test($("#username2").val())){
			
				alert("昵称只能为中文！");
			}
			
			else	if($("#password").val()==""){
				alert("密码不能为空");
			}
			else if(!reg_password.test($("#password").val())){
				alert("必须包括字母和数字可以包括指定特殊字符长度9--15之间");
				
			}
			else	if($("#password2").val()!=$("#password").val()){
				alert("两次密码不一样，请重新输入");
			}
			else	if($("#code").val()==""){
				alert("请输入验证码");
			}
			else{
				//alert("准备注册！");
				 //  var name=editor.txt.html();
				      // var role={"name":name,"film":film};            //新建一个json对象
				       var url="register.do";
				  $.post(
				           url,
				           
				        /*    {"data":JSON.stringify(name)}, */
				        {
				        
				        	 phonenumber:$("#username").val(),
				        	name:$("#username2").val(),
				        	password:$("#password").val(),
				        	code:$("#code").val() 
				        	
				        	//data:"test测试"
				        },
				           function(data,status) {
				        	  alert(data+"  状态:"+status);
				        //	  $("#username").val("");
					   //          $("#password").val(""); 
					   //          $("#password2").val(""); 
					   //          $("#code").val("");
				             //  document.getElementById("te1").innerHTML=data;
				             
				        	   if(data=="注册成功！"){
				        		   $("#username").val("");
								   $("#password").val(""); 
								    $("#password2").val(""); 
								    $("#code").val("");
							       }
				           });
			}
			
			
			
			});
		});
		
		
		</script>
	</body>
</html>