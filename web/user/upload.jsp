<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.dao.entiy.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="http://wan.wan.group/picture/a.jpg" type="image/x-icon"><!-- 在网页标题左侧显示图标 -->
    <link rel="shortcut icon" href="http://wan.wan.group/picture/a.jpg" type="image/x-icon"><!-- 在收藏夹显示图标 -->
    <meta name="keywords" content="个人博客,WEB学习,WEB学习博客,XXX个人博客" />
    <meta name="description" content="个人博客,WEB学习,WEB学习博客,XXX个人博客" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/base.css" rel="stylesheet">
    <link href="css/about.css" rel="stylesheet">
    <link href="css/m.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <script src="js/jquery.min.js" ></script>
    <script src="js/PersonArticle.js"></script>




    <link href="css/index.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/wangEditor.css"><!-- 用于鼠标悬浮显示下拉列表  -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.css"><!-- 点赞，收藏图标  -->
    <script src="js/scrollReveal.js"></script>
    <link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.css"><!-- 方向图标，加载图标  -->



    <title>上传文件</title>
</head>
<body>
<%
    List<User>  user=(ArrayList)session.getAttribute("user");
    //String newsNumber= (String)session.getAttribute("news");
    List<String> newsNumber= (List<String>) session.getAttribute("news");
    if(user!=null&&!user.isEmpty()){
        User user2=user.get(0);
        if (!user2.getLevel().equals("超级会员")) {//判断是否是超级用户
            response.sendRedirect("login.jsp");
        }

    }
    else {
        response.sendRedirect("login.jsp");

    }
%>





<header class="header-navigation" id="header">
    <nav>
        <div class="logo"><a href="http://121.196.147.151/a.jpg">小屋知多少</a></div>
        <h2 id="mnavh"><span class="navicon"></span></h2>
        <ul id="starlist">
            <li><a href="index.jsp">首页</a></li>
            <li><a href="upload.do">上传资源</a></li>
            <%if(newsNumber!=null&&!newsNumber.isEmpty()){if(!newsNumber.get(4).equals("0")){%>
            <li ><a href="news.do?" style="color: #e3514d">消息(<%=newsNumber.get(4)%>)</a>
            </li>
            <% }else{%>
            <li ><a href="news.do?" >消息</a>
            </li>
            <%}}else{%>
            <li ><a href="news.do?" >消息</a>
            </li>
            <%}%>
            <li><a  href="user/wangEditor.jsp">创作中心</a></li>

            <li><a href="about.jsp">关于我</a></li>

            <%
                if(user!=null&&!user.isEmpty()){
                    for(User i : user){	%>
            <li>
                <div class="dropdown">
                    <img style=" border:none;border-radius:30px;box-shadow: 0px 0px 5px #ccc;  " alt="Paris" src="<%=i.getPicture()  %>"  width="50px" height="50px">
                    <div class="dropdown-content">
                        <a href="ArticleManager.do">博文管理</a>
                        <a href="personalblog.do?name=<%=i.getName()%>">我的博客</a>
                        <a href="editcenter.do?name=<%=i.getName()%>">个人中心</a>
                        <a  href="quit.do?">退出</a>
                    </div>
                </div>
                    <%}} else {  %>
            <li><a href="login.jsp">未登录 </a><%} %>

            </li>
        </ul>
        <div class="searchbox">
            <div id="search_bar" class="search_bar">
                <form  id="searchform" action="searcharticle.do?" method="get" name="searchform" >
                    <input class="input" placeholder="想搜点什么呢.." type="text" name="keyboard" id="keyboard">
                    <input type="hidden" name="show" value="title" />
                    <input type="hidden" name="tempid" value="1" />
                    <input type="hidden" name="tbname" value="news">
                    <input type="hidden" name="Submit" value="搜索" />
                    <p class="search_ico"> <span></span></p>
                </form>
            </div>
        </div>

    </nav>
</header>
<article >
   <%-- <div >    <iframe id="zi" frameborder="0" src="News2.jsp" width="100%"   ></iframe></div>--%>
    <div style="background-image: url(../images/bg5.jpg)">
        <form action="upload.do" method="post"  enctype="multipart/form-data" >
            <input type="file" name="file" id="file">
            <input type="submit" value="上传文件" >
        </form>
       <br></br>
       <a href="downloadlist.do" > <button id="download"  >下载</button> </a>
    </div>
</article>
<footer>
    <p>Design by <a href="http://wanwan.group" target="_blank">c小白c| &nbsp 备案号：</a> <a href="https://beian.miit.gov.cn/#/Integrated/recordQuery" target="_blank">粤ICP备2021050792号</a></p>
</footer>
<a href="#" class="cd-top">Top</a>
<style>
    .copyrights{text-indent:-9999px;height:0;line-height:0;font-size:0;overflow:hidden;}
</style>

<script>

    $("#zi").height(screen.availHeight);

</script>
</body>
</html>