<%@page import="com.dao.entiy.StatisticalList"%>
<%@page import="com.dao.entiy.UserComment"%>
<%@page import="com.dao.entiy.Article"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.dao.entiy.User"%>
<!doctype html>
<html lang="zh-CN">
<head>    <meta charset="utf-8">
    <meta name="author" content="order by dede58.com/" /><meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/nprogress.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script src="js/jquery.lazyload.min.js"></script>
    
    <link rel="icon" href="http://wanwan.group/picture/a.jpg" type="image/x-icon"><!-- 在网页标题左侧显示图标 -->
	<link rel="shortcut icon" href="http://wanwan.group/picture/a.jpg" type="image/x-icon"><!-- 在收藏夹显示图标 -->

    <link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.css"><!--点赞，收藏图标  -->

    <style>
        div.center {text-align: center;}
    </style>
</head>
<body >

 <%
		List<Article>  list=(ArrayList<Article>)request.getAttribute("personarticle");
	%>
		<%
			if(list!=null&&!list.isEmpty()){
				for(Article i : list){		
		%>

 <div class="user-select single" style="padding-top: 20px">
    <section class="container">
  <div class="content-wrap">
    <div class="content">
      <header class="article-header">
        <h1 class="article-title"><a  href="personalblog.do?name=<%=i.getName()%>" target="_blank" title="<%=i.getTitle() %>" >  <%=i.getTitle() %></a></h1>
        <div class="article-meta">
            <input type="hidden" id="author" value="<%=i.getName()%>">
            <a href="personalblog.do?name=<%=i.getName()%>">
        <span class="item article-meta-source" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="作者：<%=i.getName() %>"><i class="glyphicon glyphicon-globe"></i>作者：<span  id="name"><%=i.getName() %></span></span></a>
        <span class="item article-meta-time"><time class="time" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="发表时间：<%=i.getTime()%>"><i class="glyphicon glyphicon-time"></i> <%=i.getTime() %></time>
          </span>  <span class="item article-meta-category" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="MZ-NetBlog主题"><i class="glyphicon glyphicon-list"></i> <a href="http://www.muzhuangnet.com/list/mznetblog/" title="<%=i.getArticlelabel() %>" ><%=i.getArticlelabel()%></a></span> <span class="item article-meta-views" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="浏览量：<%=i.getPageview() %>"><i class="glyphicon glyphicon-eye-open"></i><%=i.getPageview() %></span> <span class="item article-meta-comment" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="评论量"><i class="glyphicon glyphicon-comment"></i> <%=i.getComment() %></span> </div>
      </header>
      <article class="article-content">
        
 <div>
 <%=i.getContent() %>
 
 
 
 </div>
 	<input type="hidden" id="title" value=<%=i.getTitle() %>>
 	<input type="hidden" id="titleid" value=<%=i.getId() %>>
 </article>
 
    <%
      StatisticalList  sList2=(StatisticalList)request.getAttribute("messagelist");
	%>
		<%
			if(sList2!=null){   %>
			<div  id="likecollect">
			<%if(sList2.getLikestatus()!=null){   %>
     <span id="nolike" style="color:red;"> <i class="fa fa-thumbs-o-up" style="font-size:24px"></i> <span> <%=sList2.getLikestatus() %>（<span id="likenumber"><%=i.getLikenumber() %></span>）</span></span>
     <%}else{ %>
     <span id="like"> <i class="fa fa-thumbs-o-up" style="font-size:24px"></i> 点赞（<span id="likenumber"><%=i.getLikenumber() %></span>）</span>
       <%} %>
     <span> &nbsp </span><span>&nbsp   </span>
     <%  if(sList2.getCollectstatus()!=null){   %>
     <span id="nocollect" style="color: red;"> <i class="fa fa-star" style="font-size:24px"></i><%=sList2.getCollectstatus() %>（<span id="collectnumber"><%=i.getCollect()%> </span>）</span>
     <%}else{ %>
          <span id="collect"><i class="fa fa-star" style="font-size:24px"></i>收藏（<span id="collectnumber"><%=i.getCollect()%></span>） </span>
     <%} %>
      </div>
      <%} else{%>
      	<div  id="likecollect">
     <span id="like"> <i class="fa fa-thumbs-o-up" style="font-size:24px"></i> 点赞（<span id="likenumber"><%=i.getLikenumber() %></span>）</span>
     <span> &nbsp </span><span>&nbsp   </span> 
     <span id="collect"><i class="fa fa-star" style="font-size:24px"></i>收藏（<span id="collectnumber"><%=i.getCollect()%></span>） </span>
      </div>
      
      <%} %>
      <div class="title" id="comment">
       <h3>评论</h3>
      </div>
      <div id="respond">
         <!--    <form id="comment-form" name="comment-form" action="" method="POST"> -->
                <div class="comment">
                   <!--  <input name="" id="" class="form-control" size="22" placeholder="您的昵称（必填）" maxlength="15" autocomplete="off" tabindex="1" type="text">
                    <input name="" id="" class="form-control" size="22" placeholder="您的网址或邮箱（非必填）" maxlength="58" autocomplete="off" tabindex="2" type="text"> -->
                    <div class="comment-box">
                        <textarea placeholder="您的评论或留言" name="comment-textarea" id="comment-textarea" cols="100%" rows="3" tabindex="3" id="comment"></textarea>
                        <div class="comment-ctrl">
<%--
                            <div class="comment-prompt" style="display: none;"> <i class="fa fa-spin fa-circle-o-notch"></i> <span class="comment-prompt-text">评论正在提交中...请稍后</span> </div>
--%>
                            <div class="comment-success" style="display: none;"> <i class="fa fa-check"></i> <span class="comment-prompt-text">评论提交成功...</span> </div>
                            <button type="submit" name="comment-submit" id="comment-submit" tabindex="4"  onclick="comment()">评论</button>
                        </div>
                    </div>
                </div>
           <!--  </form> -->
            
        </div>
      <div id="postcomments">
        <ol id="comment_list" class="commentlist" >   
       <%
		List<UserComment>  userComments=(ArrayList)request.getAttribute("userComments");
	%>
		<%
			if(userComments!=null&&!userComments.isEmpty()){
				for(UserComment userComment: userComments){		
		%>      
        <li id="commentli" class="comment-content"><span class="comment-f"></span><div class="comment-main"><p><a class="address" href="personalblog.do?name=<%=userComment.getName()%>" rel="nofollow" target="_blank"><%=userComment.getName() %></a><span class="time"><%=userComment.getTime() %></span><br><%=userComment.getComment() %></p></div></li>
      <!--   <li class="comment-content"><span class="comment-f">#1</span><div class="comment-main"><p><a class="address"  rel="nofollow" target="_blank">XXX网络博客</a><span class="time">(2016/10/14 21:02:39)</span><br>博客做得好漂亮哦！</p></div></li> -->
    
     <%}
				}else{
					%>
				        <li style="display: none" id="commentli" class="comment-content"><span class="comment-f"></span><div class="comment-main"><p><a class="address"  rel="nofollow" target="_blank"></a><span class="time"></span><br></p></div></li>
					
			<%} %>	
			  </ol>
      </div>
      
    </div>
  </div>
  <aside class="sidebar">
    <div class="fixed">
    
    <div class="widget widget_search">
        <form class="navbar-form" action="searcharticle.do?" method="get" target="_blank">
          <div class="input-group">
            <input type="text" name="keyboard" class="form-control" size="35" placeholder="请输入关键字" maxlength="15" autocomplete="off">
            <span class="input-group-btn">
            <button class="btn btn-default btn-search" name="search" type="submit">搜索</button>
            </span> </div>
        </form>
      </div>
      <%
      StatisticalList  sList=(StatisticalList)request.getAttribute("usermessage");
	%>
		<%
			if(sList!=null){   
				%>

			
      <div class="widget widget-tabs">
     <!--   <ul class="nav nav-tabs" role="tablist"> -->
        <ul class="nav " role="tablist" style="padding-left: 35%;padding-top: 10%;padding-right: 35%;" >
         <!--  <li role="presentation" class="active"><a href="#notice" aria-controls="notice" role="tab" data-toggle="tab" draggable="false">统计信息</a></li> -->
        <li> <a href="personalblog.do?name=<%=i.getName()%>"><img style=" border:none;border-radius:30px;box-shadow: 0px 0px 5px #ccc;  " alt="Paris" src="<%=sList.getPicture()  %>"  width="50px" height="50px">   <%=sList.getName() %></a></li>
        
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane contact active" id="notice">
         <ul>
                      <li class="comment-content"><div class="comment-main"><p id="guanzhu"><a >访问：<%=sList.getPageviewsum() %></a><span >评论:<span id="newcomment"><%=sList.getCommentsum() %></span></span><span> &nbsp 收藏:<span id="newcollect"><%=sList.getCollectsum() %></span></span><span> &nbsp 点赞:<span id="newlike"><%=sList.getLikesum() %></span></span><span> &nbsp 粉丝:<span id="newfans"><%=sList.getFanssum() %></span></span>
                      <%if(sList2!=null){   %>
                      <%if(sList2.getFasstatus()!=null){  %>
                      <span  id="nofans" style="size: 20px;color: red;">&nbsp &nbsp <%=sList2.getFasstatus() %></span>
                      <%}else{ %>
                      <span id="fans" style="size: 20px;color: red;">&nbsp &nbsp 关注</span>
                      <%} %>
				<%}else{ %>
				<span id="fans" style="size: 20px;color: red;">&nbsp &nbsp 关注</span>
				<%} %>
				</p></div></li> 
               </ul> 
           
          </div>
          
        </div>
      </div>
 <%  }  %>   
 
    </div>
    
    <div class="widget widget_hot">
          <h3>最新文章</h3>
          <ul  >
            
<%
		List<Article>  newarticle=(ArrayList<Article>)request.getAttribute("newarticle");
	%>
		<%
			if(newarticle!=null&&!newarticle.isEmpty()){
				for(Article newarticle2: newarticle){			
		%>
	<%-- 	onclick="personarticle(<%=newarticle2.getId()%>,'<%=newarticle2.getName()%>')" --%>
	
<li ><a  target="_blank" title="<%=newarticle2.getTitle() %>" href="personarticle.do?cam223c=1&cnmnam=5&cbfbahf=5&caxsx=5&articleid=<%=newarticle2.getId()%>&name=<%=newarticle2.getName()%>"><span class="thumbnail">
    <span class="thumb"   style="display: block;"><%=newarticle2.getTitle() %></span>
    <input type="hidden" value="<%=newarticle2.getId()%>"  >
     <input type="hidden" value="<%=newarticle2.getName()%>" >
</span><div class="text"  style=" max-height: 150px;overflow: hidden;text-overflow: ellipsis;hite-space: nowrap;"><%=newarticle2.getContent() %></div>
<span class="muted"><i class="glyphicon glyphicon-time"></i>
   <%=newarticle2.getTime() %>
</span><span class="muted"><i class="glyphicon glyphicon-eye-open"></i><%=newarticle2.getPageview() %></span></a></li>
<%} }%>

</ul>
      </div>
  
  </aside>
</section>

 <div class="center">
  <p>Design by <a href="http://wanwan.group" target="_blank">c小白c| &nbsp 备案号：</a> <a href="https://beian.miit.gov.cn/#/Integrated/recordQuery" target="_blank">粤ICP备2021050792号</a></p>
 </div >

    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.ias.js"></script>
    <script src="../js/scripts.js"></script>
    	<script type="text/javascript">
		document.title=$("#title").val()
		</script>
			<% 	
				}
			}
		%> 
		
		
		
		
	<script type="text/javascript">
	function comment() {//用户进行对文章的评论
	
	if($("#comment-textarea").val()==""){
		alert("内容不能为空");
		
	}
	else{
		  var url="comment.do"; 
		
		  $.post(
		           url,
		      
		        {
		        	 comment:$("#comment-textarea").val(),  //评论的内容
		        	 title:$("#title").val(),//文章标题
		        	 titleid:$("#titleid").val(),		//文章id
                     author:$("#author").val()
		        	
		        },
		            function(data,status) {
		        	//  alert(data+"  状态:"+status);
		       
		       if(data=="请先登陆！"){
		      alert("请先登录后再评论");
		  
		       }
		       else if(data=="评论失败！"){
		    	   alert("评论失败！");
		    	   
		       }
		       else if(data=="请勿频繁评论！"){
		    	   alert("请勿频繁评论！");
		       }
		       else{
		    	//   alert("评论成功！");
		    	   var json = JSON.parse(data);//获取json数据
		    	   var comment="<li id=commentli class=comment-content><span class=comment-f></span><div class=comment-main><p><a class=address  rel=nofollow target=_blank>"+json[0].name+"</a><span class=time>"+json[0].time+"</span><br>"+json[0].comment+"</p></div></li>"
				       $("#commentli").before(comment);
		    	       $("#newcomment").text(parseInt($("#newcomment").text())+1);//刷新数量
				        	 
		       }
		 /*    var json = JSON.parse(data);
		    var x;
		    for (i in json) {
		        x += json[i].content + "<br>";
		    }
		        	  $("#test1").html(x);  */
		       $("#comment-textarea").val("");
		        }
		        );  
	        
	}	
	}
	
	
	
	/* 
	用户对文章进行点赞，收藏，关注等功能
	
	 */
	$(document).ready(function(){
		var url;
		  
		  function intermediary(url) {
			  $.post(
			           url,
			      
			        {
			        	 name:$("#name").text(),//作者姓名
                         title:$("#title").val(),//文章标题
			        	 titleid:$("#titleid").val()		//文章id       
			        	
			        },
			            function(data,status) {
			        	//  alert(data+"  状态:"+status);
			       
			       if(data=="请先登陆！"){
			      alert("请先登录");
			  
			       }
			       else if(data=="失败！"){
			    	   alert("操作失败！");
			    	   
			       }
			       
			       
			       else if(data=="成功！"){
			    	   
			    	  // alert("成功");
			    if(url=="like.do"){
			    	var num=parseInt($("#likenumber").text())+1;
			  var txt="  <span id=\"nolike\"  style=\"color: red;\"> <i class=\"fa fa-thumbs-o-up\" style=\"font-size:24px;\"></i>  已点赞（<span id=\"likenumber\">"+num +"</span>）</span>";
			  $("#like").remove();
				 $("#likecollect").prepend(txt);
				 $("#newlike").text(parseInt($("#newlike").text())+1);//刷新数量
				 	
			
			    }
			    if(url=="nolike.do"){
			    	var num=parseInt($("#likenumber").text())-1;
					var txt="  <span id=like> <i class=\"fa fa-thumbs-o-up\" style=\"font-size:24px;\"></i>  点赞（<span id=\"likenumber\">"+num +"</span>）</span>";
					$("#nolike").remove();
					 $("#likecollect").prepend(txt);
					 $("#newlike").text(parseInt($("#newlike").text())-1);//刷新数量
						 	
					
			    }
			    if(url=="collect.do"){
			    	  var num=parseInt($("#collectnumber").text())+1;
					  var txt="<span id=nocollect   style=\"color: red;\"> <i class=\"fa fa-star\" style=\"font-size:24px;\"></i>已收藏（<span id=\"collectnumber\">"+num+"</span>）</span>";
					  $("#collect").remove();
					  $("#likecollect").append(txt);
					  $("#newcollect").text(parseInt($("#newcollect").text())+1);//刷新数量
					    }
 				if(url=="nocollect.do"){
 					  var num=parseInt($("#collectnumber").text())-1;
					  var txt="<span id=collect> <i class=\"fa fa-star\" style=\"font-size:24px;\"></i>收藏（<span id=\"collectnumber\">"+num+"</span>）</span>";
					  $("#nocollect").remove();
					  $("#likecollect").append(txt);
					  $("#newcollect").text(parseInt($("#newcollect").text())-1);//刷新数量
			    }
			    
			    if(url=="fans.do"){
			    	
			    	var txt="<span  id=nofans style=\"size: 20px;color: red;\">&nbsp &nbsp 已关注</span>";
			  	  $("#fans").remove();
				  $("#guanzhu").append(txt);
				  $("#newfans").text(parseInt($("#newfans").text())+1);//刷新数量
			    }
				if(url=="nofans.do"){
					
					var txt="<span  id=fans style=\"size: 20px;\">&nbsp &nbsp 关注</span>";
				  	  $("#nofans").remove();
					  $("#guanzhu").append(txt);
					  $("#newfans").text(parseInt($("#newfans").text())-1);//刷新数量
			    }
			    
			    
			       }
			       
			       
			        }
			        );  	
			  }
 		  $("#likecollect").on("click","#nolike",function(){//取消点赞
			  
 			//  alert("nolike");
 			 url="nolike.do"; 
			 intermediary(url);
 		  });
 		 $("#likecollect").on("click","#nocollect",function(){//取消收藏
			  
			//  alert("nocollect");
			  url="nocollect.do"; 
   		      intermediary(url);
		  });
       $("#likecollect").on("click","#like",function(){//点赞
			  
 			//  alert("like");
 			  url="like.do"; 
			 intermediary(url);
 		  });
 		 $("#likecollect").on("click","#collect",function(){//收藏
			  
			//  alert("collect");
			  url="collect.do"; 
		      intermediary(url);
		  });
 		
 		 $("#guanzhu").on("click","#fans",function(){//关注
			  
			 // alert("fans");
			  url="fans.do"; 
			   intermediary(url);
		      
		  });
 		$("#guanzhu").on("click","#nofans",function(){//取消关注
			  
			//  alert("nofans");
			  url="nofans.do"; 
			  intermediary(url);
		      
		  });

		  
		  
		  
		});
	
	
	</script>
</body>
</html>
