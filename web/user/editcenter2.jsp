<%@ page import="com.dao.entiy.User" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/5/25
  Time: 21:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>个人中心</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        /* Custom Styles */
        ul.nav-tabs {
            width: 140px;
            margin-top: 20px;
            border-radius: 4px;
            border: 1px solid #ddd;
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.067);
        }

        ul.nav-tabs li {
            margin: 0;
            border-top: 1px solid #ddd;
        }

        ul.nav-tabs li:first-child {
            border-top: none;
        }

        ul.nav-tabs li a {
            margin: 0;
            padding: 8px 16px;
            border-radius: 0;
        }

        ul.nav-tabs li.active a, ul.nav-tabs li.active a:hover {
            /*   color: #fff;
               background: #0088cc;
               border: 1px solid #0088cc;*/
        }

        ul.nav-tabs li:first-child a {
            border-radius: 4px 4px 0 0;
        }

        ul.nav-tabs li:last-child a {
            border-radius: 0 0 4px 4px;
        }

        ul.nav-tabs.affix {
            top: 30px; /* Set the top position of pinned element */
        }

    </style>
</head>

<body data-spy="scroll" data-target="#myScrollspy">
<%--
<div style="margin-bottom: 0;"> <iframe id="zi" frameborder="0" src="user/editcenter.jsp" width="100%"  height="70px" ></iframe></div>
--%>

<%
    List<User> user = (ArrayList) session.getAttribute("user");

%>
<div class="container" style="margin-left: 5%;margin-top: 30px">

    <div class="row">
        <div class="col-xs-3" id="myScrollspy">
            <ul class="nav nav-tabs nav-stacked" data-spy="affix" data-offset-top="125">
                <li class="active" id="one"><a href="#section-1">个人资料</a></li>
                <li id="two"><a href="#section-2">账号设置</a></li>
                <li id="three"><a href="#section-3">我的粉丝</a></li>
                <li id="four"><a href="#section-4">我的关注</a></li>
                <li id="five"><a href="#section-5">我的收藏</a></li>
            </ul>
        </div>
        <%
            if (user != null && !user.isEmpty()) {
                for (User i : user) {
        %>
        <div class="col-xs-9" style="margin-top: 50px;">
            <div id="section-1">
                <br/>
                <div class="list-group">
                    <a href="#" class="list-group-item active">
                        <small style="width:8%;"><button  style="width:8%;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" style="height: 100px;width: 100px;">
                        <img style=" border:none;border-radius:80px;box-shadow: 0px 0px 5px #ccc; " alt="Paris"
                             src="..<%=i.getPicture()%>" width="100px" height="100px">
                        </button> </small>
                    </a>
                    <div class="list-group-item">
                        <h4 > 用户昵称： <span id="name"><%=i.getName()%></span>
                        </h4>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            用户账号：<%=i.getPhonenumber()%>
                        </h4>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            用户邮箱：  <%=i.getMailbox()%>
                        </h4>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            用户等级：  <%=i.getLevel()%>
                        </h4>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            当前登录时间：<%=i.getLogintime()%>
                        </h4>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            上次退出时间：  <%=i.getOuttime()%>
                        </h4>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            注册时间：  <%=i.getRegistertime()%>
                        </h4>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            登录频次：  <%=i.getFrequency()%>
                        </h4>
                    </div>


                </div>
            </div>
            <div id="section-2" style="display: none">
                <br/>
                <div class="list-group">
                  <%--  <a href="#" class="list-group-item active">
                        账号设置
                    </a>--%>
                    <div class="list-group-item">
                        <h4> 手机号： <%=i.getPhonenumber()%>&nbsp;&nbsp;<small style="color: #09fffc;">修改手机</small></h4>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            <%if(i.getMailbox()==null){%>
                            邮 箱：<%=i.getMailbox()%>&nbsp;&nbsp;<small style="color: #09fffc;"  data-toggle="modal" data-target="#myModal2"> 绑定邮箱</small>
                            <%}else{%>
                            邮 箱：<%=i.getMailbox()%>&nbsp;
                            <%}%>
                        </h4>
                    </div>
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            密 码：***&nbsp;&nbsp;<small style="color: #09fffc;"  data-toggle="modal" data-target="#myModal3">更改密码</small>
                        </h4>
                    </div>


                </div>
            </div>
            <div id="section-3" style="display: none">


            </div>
            <div id="section-4" style="display: none;">


            </div>
            <div id="section-5" style="display: none">


            </div>
        </div>
    </div>
</div>


<!-- 模态框（Modal） -->
<div   style="padding-top:10%; "class="modal fade"  data-backdrop="static" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                   更改头像
                </h4>
            </div>
            <div class="modal-body" >

                <form action="uploadpicture.do" method="post"  enctype="multipart/form-data" >
                    <input type="file" name="file" id="file" >
                    </br>
                    <input type="submit" value="上传头像"  >
                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" >关闭
                </button>
               <%-- <button type="button" class="btn btn-primary"   data-dismiss="modal" id="send">
                    提交更改
                </button>--%>
            </div>
        </div>
    </div>
</div>



<!-- 模态框（Modal） -->
<div   style="padding-top:10%; "class="modal fade"  data-backdrop="static" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel2">
                   绑定邮箱
                </h4>
            </div>
            <div class="modal-body" >
                <form  >
                  你的邮箱:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="email" id="email"  required>
                </form><br/>
                <button  onclick="sendEmail('email')">获取邮箱验证码</button>
                  <input type="text" id="code" placeholder="填写验证码" ><br/><br/><input onclick="saveEmail()" type="submit" value="绑定邮箱">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" >关闭
                </button>
                <%-- <button type="button" class="btn btn-primary"   data-dismiss="modal" id="send">
                     提交更改
                 </button>--%>
            </div>
        </div>
    </div>
</div>


<!-- 模态框（Modal） -->
<div   style="padding-top:10%; "class="modal fade"  data-backdrop="static" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel3">
                   更改密码
                </h4>
            </div>
            <div class="modal-body" >

                <form  >
                    绑定的邮箱:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="email" id="email2"  required>
                </form><br/>
                <button  onclick="sendEmail('email2')">获取邮箱验证码</button>
                <input type="text" id="code2" placeholder="填写验证码" ><br/><br/>
                <form> 新密码：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="password" id="password" value=""><br/><br/>
                    确认密码：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="password"  id="password2" value=""></form><br/><input onclick="updatepassword()" type="submit" value="确认更改密码">



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" >关闭
                </button>
                <%-- <button type="button" class="btn btn-primary"   data-dismiss="modal" id="send">
                     提交更改
                 </button>--%>
            </div>
        </div>
    </div>
</div>


<% }
}else{
    response.sendRedirect("../user/error.html");
}%>

<script>


    $(document).ready(function () {

        $("#one").click(function () {
            this.style.backgroundColor = "#0088cc";
            $("#one").css("color", "white");
            $("#two").css("backgroundColor", "white");
            $("#three").css("backgroundColor", "white");
            $("#four").css("backgroundColor", "white");
            $("#five").css("backgroundColor", "white");
            $("#section-1").show();
            $("#section-2").hide();
            $("#section-3").hide();
            $("#section-4").hide();
            $("#section-5").hide();
        });
        $("#two").click(function () {
            $("#section-2").show();
            $("#section-1").hide();
            $("#section-3").hide();
            $("#section-4").hide();
            $("#section-5").hide();
            this.style.backgroundColor = "#0088cc";
            $("#two").css("color", "white");
            $("#one").css("backgroundColor", "white");
            $("#three").css("backgroundColor", "white");
            $("#four").css("backgroundColor", "white");
            $("#five").css("backgroundColor", "white");
        });
        $("#three").click(function () {
            $("#section-3").show();
            $("#section-2").hide();
            $("#section-1").hide();
            $("#section-4").hide();
            $("#section-5").hide();
            this.style.backgroundColor = "#0088cc";
            $("#three").css("color", "white");
            $("#two").css("backgroundColor", "white");
            $("#one").css("backgroundColor", "white");
            $("#four").css("backgroundColor", "white");
            $("#five").css("backgroundColor", "white");
            send("fans");

        });
        $("#four").click(function () {
            $("#section-4").show();
            $("#section-2").hide();
            $("#section-3").hide();
            $("#section-1").hide();
            $("#section-5").hide();
            this.style.backgroundColor = "#0088cc";
            $("#four").css("color", "white");
            $("#two").css("backgroundColor", "white");
            $("#three").css("backgroundColor", "white");
            $("#one").css("backgroundColor", "white");
            $("#five").css("backgroundColor", "white");
            send("guanzhu");
        });
        $("#five").click(function () {
            $("#section-5").show();
            $("#section-2").hide();
            $("#section-3").hide();
            $("#section-4").hide();
            $("#section-1").hide();
            this.style.backgroundColor = "#0088cc";
            $("#five").css("color", "white");
            $("#two").css("backgroundColor", "white");
            $("#three").css("backgroundColor", "white");
            $("#one").css("backgroundColor", "white");
            $("#four").css("backgroundColor", "white");
            send("collect");
        });

        function send(check) {
           // alert(check);
            $("#load").remove();
            var url;
            if (check == "fans") {

                url = "lookfans.do";
                $("#section-3").append('<img id="load" src="../images/load.gif"/>');
            }
            if (check == "guanzhu") {

                url = "lookguanzhu.do";
                $("#section-4").append('<img id="load" src="../images/load.gif"/>');
            }
            if (check == "collect") {

                url = "lookcollect.do";
                $("#section-5").append('<img id="load" src="../images/load.gif"/>');
            }
            $.post(
                url,

                {
                 name:$("#name").text()
                },

                function (data) {


                    if (data == "失败了！") {
                        alert("没有数据了")
                        $("#load").remove();
                    } else {
                        for (var l = 0; l < 55; l++) {
                            $("#load").remove();
                            $("#fans").remove();
                            $("#guanzhu").remove();
                            $("#collect").remove();
                        }
                        json = JSON.parse(data);
                        // for(var i=0;i<5;i++){
                        for (i in json) {
                            if (check == "fans") {
                                var txt2;
                                txt2 = " <div class=\"list-group-item\" id=\"fans\"> <h4>  粉丝昵称:&nbsp;&nbsp;<a target=\"_parent\" href=\"../personalblog.do?name="+json[i].fans+"\"><span onclick=\"x1('kk')\" id=\"kk\" style=\"color: #ea33a7;\">"+json[i].fans+"</span></a>&nbsp;&nbsp;<small style=\"color: #09fffc;\">时间：" + json[i].time + "</small></h4></div>";
                                $("#section-3").append(txt2);
                            }
                            if (check == "guanzhu") {
                                txt2 = " <div class=\"list-group-item\" id=\"guanzhu\"> <h4>  关注的人:&nbsp;&nbsp;<a target=\"_parent\" href=\"../personalblog.do?name="+json[i].name+"\"><span \ style=\"color: #ea33a7;\">" +json[i].name + "</span></a>&nbsp;&nbsp;<small style=\"color: #09fffc;\">时间：" + json[i].time + "</small></h4></div>";
                                $("#section-4").append(txt2);
                            }
                            if (check == "collect") {
                                txt2 = " <div class=\"list-group-item\" id=\"collect\"> <h4>  收藏的文章标题:&nbsp;&nbsp;<a target=\"_blank\" href=\"../personarticle.do?cam223c=1&cnmnam=5&cbfbahf=5&caxsx=5&articleid="+json[i].titleId+"&name="+json[i].name+"\"><span style=\"color: #ea33a7;\">" +json[i].title+ "</span></a>&nbsp;&nbsp;<small style=\"color: #09fffc;\">时间：" + json[i].time + "</small></h4></div>";
                                $("#section-5").append(txt2);
                            }


                        }
                    }

                }
            );

        }




    });
    function sendEmail(check) {
        if(check=="email"){
            myemail=$("#email").val();
            var url="bindingEmail.do";
        }
        if(check=="email2"){
            myemail=$("#email2").val();
            var url="bindingEmail2.do";
        }

        $.post(
            url,

            {
                email:myemail,
                name:$("#name").text(),
                code:$("#code").val()

            },
            function (data) {
               // alert(data);
                if (data == "false") {
                    alert("发送失败")

                }
                else if(data=="邮箱格式不正确"){
                    alert("邮箱格式不正确");
                }
                else if(data=="exist"){
                    alert("你已发送验证码，一分钟内请勿重复操作")
                }

             else  if(data=="sucessful") {
                    alert("验证码已发送");

                }
             else{
                 alert(data);
                }


            }
        );
    }
    function saveEmail() {

        var url="saveEmail.do";
        $.post(
            url,

            {
                email:$("#email").val(),
                name:$("#name").text(),
                code:$("#code").val()

            },
            function (data) {
                alert(data);


            }
        );
    }

    function updatepassword() {
        var reg_password= /^(?![^a-zA-Z]+$)(?!\D+$)[a-zA-Z0-9!@#$%]{8,15}$/;  //密码正则
      	if($("#code2").val()==""){
            alert("请输入验证码");
        }
        else if($("#password").val()==""){
            alert("密码不能为空");
        }
        else if(!reg_password.test($("#password").val())){
            alert("必须包括字母和数字可以包括指定特殊字符长度9--15之间");

        }
        else	if($("#password2").val()!=$("#password").val()){
            alert("两次密码不一样，请重新输入");
        }
       else{
           var tf=confirm("确定更改密码吗？");
           if(tf){
              // alert(tf);

        var url="udatepassword.do";
        $.post(
            url,

            {
                password:$("#password").val(),
                password2:$("#password2").val(),
                name:$("#name").text(),
                code2:$("#code2").val(),
                email:$("#email2").val()

            },
            function (data) {

                if(data==""){
                    alert("验证码过期");
                }
                else{
                    alert(data);
                }

            }
        );
           }


        }
    }

</script>
</body>
</html>