package com.dao.Impl;
/*
用于保存用户发布的文章，以及查询文章库
*/

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.MediaSize.NA;

import com.dao.entiy.*;
import com.dao.utils.JDBCMySql;


public class WangEditor {
    private JDBCMySql jdbc=JDBCMySql.getJDBC();		   //调用封装好的JDBC




    /*
     *
     *
     * 保存文章
     *
     *
     */
    public int add(Article article,String check) {//保存文章
        //设置添加sql语句
        String sql=null;
        if(check.equals("新文章")) {
           sql = "insert into article(name,phonenumber,title,content,articlelabel,subfield,articletype,modality,time,pageview,likenumber,comment,collect) values('" + article.getName() + "','" + article.getPhonenumber() + "','" + article.getTitle() + "','" + article.getContent() + "','" + article.getArticlelabel() + "','" + article.getSubfield() + "','" + article.getArticletype() + "','" + article.getModality() + "',now(),0,0,0,0)";
        }
        if(check.equals("更新文章")){
            sql="update article set title='"+article.getTitle()+"', content='"+article.getContent()+"', articlelabel='"+article.getArticlelabel()+"', subfield='"+article.getSubfield()+"', articletype='"+article.getArticletype()+"', modality='"+article.getModality()+"',status='待审核' where id='"+article.getId()+"'";
        }
        int n=jdbc.submitSQL(sql);					    //提交
        return n;

    }



    /*
     *
     *
     * 查询文章
     *
     */
    public List<Article> query(String s,String id,int n) {//查询文章
        List<Article> list=new ArrayList<Article>();
        Article article=null;
        String sql=null;
        int number=5*(n-1);
        //String sql="select * from user  where phonenumber='"+phonenumber+"'and password='"+password+"'";
        if(s.equals("article")) {
            sql="select * from article where modality='公开' and status='通过审核' order by id desc LIMIT 5;";//查询最近的前一百条文章
        }
        if(s.equals("hotArticle")) {
            //select id,name,pageview from article GROUP BY pageview  desc;
            sql="select * from article where modality='公开' and status='通过审核' GROUP BY pageview  desc LIMIT "+number+",5";//查询数量最大的前一百条文章
        }
        if(s.equals("newArticle")) {
            //select id,name,pageview from article GROUP BY pageview  desc;
            sql="select * from article where modality='公开' and status='通过审核' order by id  desc LIMIT "+number+",5";//查询数量最大的前一百条文章
        }

        if(s.equals("personarticle")) {
            sql="select * from article where id='"+id+"' and modality='公开' and status='通过审核' ";//查询个人文章
            //String logintime="update  article set pageview=pageview+1  where id='"+id+"'";//更新文章浏览量
            //int n=jdbc.submitSQL(logintime);	//执行更新语句
            // sql="select * from article where id=1";
        }
        if(s.equals("articleEditor")) {
            sql="select * from article where id='"+id+"' ";//查询个人文章

        }
        //select * from article order by id desc LIMIT 20;//查询最新的二十条信息
        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {

            try {
                while(rs.next()) {
                    article=new Article();
                    article.setId(rs.getInt("id"));
                    article.setName(rs.getString("name"));
                    article.setPhonenumber(rs.getString("phonenumber"));
                    article.setTitle(rs.getString("title"));
                    article.setContent(rs.getString("content"));
                    article.setArticlelabel(rs.getString("articlelabel"));
                    article.setSubfield(rs.getString("subfield"));
                    article.setArticletype(rs.getString("articletype"));
                    article.setModality(rs.getString("modality"));
                    article.setTime(rs.getString("time"));
                    article.setPageview(rs.getInt("pageview"));
                    article.setLikenumber(rs.getInt("likenumber"));
                    article.setComment(rs.getInt("comment"));
                    article.setCollect(rs.getInt("collect"));
                    list.add(article);

                }
                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    /*
     *
     *
     *向数据库保存用户对文章的评论
     *
     *
     *
     */
    public int addcomment(String phonenumber,String name,String comment,String titleid,String title,String author) {//向数据库保存用户对文章的评论
        //设置添加sql语句

        String sql="insert into articlecomment(phonenumber,name,comment,title,titleid,time,author) values('"+phonenumber+"','"+name+"','"+comment+"','"+title+"','"+titleid+"',now(),'"+author+"')";
        int n=jdbc.submitSQL(sql);						    //提交
        if(n==1) {
            jdbc.submitSQL("update article set comment=comment+1 where id='"+titleid+"'");//更新评论数量
        }
        return n;
    }

    /*
     *
     *
     * 查询文章评论
     *
     */
    public List<UserComment> coment(String titleid) {//查询文章评论
        List<UserComment> list=new ArrayList<UserComment>();
        UserComment userComment=null;

        String sql="select * from articlecomment where titleid='"+titleid+"' order by id desc ;";

        //select * from article order by id desc LIMIT 20;//查询最新的二十条信息
        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {

            try {
                while(rs.next()) {
                    userComment=new UserComment();
                    userComment.setName(rs.getString("name"));
                    userComment.setComment(rs.getString("comment"));
                    userComment.setTime(rs.getString("time"));
                    list.add(userComment);

                }
                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }


    /*
     *
     *
     *
     *
     *查询文章的作者信息
     *
     *
     *
     */
    public StatisticalList articleperson(String name) {//查询文章的作者信息
        //String sql="select sum(pageview) from article where phonenumber='"+name+"' union all select count(fans) from fanslist where phonenumber='"+name+"'";//联表查询
        //联表查询
        String sql="select  sum(pageview),sum(likenumber),sum(comment),sum(collect) from article where name='"+name+"' union all select count(fans),count(id),count(name),count(time) from fanslist where name='"+name+"' union all select name,picture,id,phonenumber from user where name='"+name+"'  union all select count(id),sum(likenumber),sum(comment),sum(collect) from article where name='"+name+"'";
        String s[]= {"","","","","","","","","","","","","","","",""};
        int num=0;
        StatisticalList statisticalList=null;
        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {

            try {
                while(rs.next()) {
                    s[num]=rs.getString(1);
                    s[++num]=rs.getString(2);
                    s[++num]=rs.getString(3);
                    s[++num]=rs.getString(4);
                    num++;

                }
                statisticalList=new StatisticalList();
                statisticalList.setPageviewsum(Integer.parseInt(s[0]));
                statisticalList.setLikesum(Integer.parseInt(s[1]));
                statisticalList.setCommentsum(Integer.parseInt(s[2]));
                statisticalList.setCollectsum(Integer.parseInt(s[3]));
                statisticalList.setFanssum(Integer.parseInt(s[4]));
                statisticalList.setName(s[8]);
                statisticalList.setPicture(s[9]);
                statisticalList.setArticlesum(Integer.parseInt(s[12]));

//			for(int i=0;i<s.length;i++) {
//				System.out.println(s[i]);
//			}
                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return statisticalList;

    }




    /*
     *
     *
     * 查询作者的五篇最新的文章
     *
     *
     */
    public List<Article> newarticle(String name,int n,String check) {
        //String sql="select sum(pageview) from article where phonenumber='"+name+"' union all select count(fans) from fanslist where phonenumber='"+name+"'";//联表查询
        int number=5*(n-1);
        System.out.println("number为"+number);//order by id desc LIMIT "+number+",5 "
        String sql=null;
        if(check!=null){
            if (check.equals("person")) {
                if (n > 1) {
                    sql = "select*from article where name='" + name + "'  order by id desc LIMIT " + number + ",5 ";
                } else {
                    sql = "select*from article where name='" + name + "'  order by id desc LIMIT 5";
                }
            }
        }
        else{
            if (n > 1) {
                sql = "select*from article where name='" + name + "' and modality='公开' and status='通过审核' order by id desc LIMIT " + number + ",5 ";
            } else {
                sql = "select*from article where name='" + name + "' and modality='公开' and status='通过审核' order by id desc LIMIT 5";
            }
        }
        Article article=null;
        List<Article> list=new ArrayList<Article>();
        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {

            try {
                while(rs.next()) {
                    article=new Article();
                    article.setTitle(rs.getString("title"));
                    article.setContent(rs.getString("content"));
                    article.setTime(rs.getString("time"));
                    article.setPageview(rs.getInt("pageview"));
                    article.setId(rs.getInt("id"));
                    article.setName(rs.getString("name"));
                    article.setModality(rs.getString("modality"));
                    article.setStatus(rs.getString("status"));
                    list.add(article);
                }

                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;

    }

    /*
     *
     *
     *
     *
     *查询·是否已被关注，点赞，收藏
     *
     *
     */
    public StatisticalList fans(String name,String othername,int articleid) {//查询·是否已被关注，点赞，收藏
        //String sql="select sum(pageview) from article where phonenumber='"+name+"' union all select count(fans) from fanslist where phonenumber='"+name+"'";//联表查询
        //联表查询
        System.out.println(name+othername+articleid);
        String sql="select count(fans) from fanslist where name='"+name+"'and fans='"+othername+"'  union all select count(*) from likelist where thelike='"+othername+"' and articleid='"+articleid+"' union  all select count(*) from collectlist where collector='"+othername+"' and articleid='"+articleid+"' ";
        String s[]= {"","",""};
        int num=0;
        StatisticalList statisticalList=null;
        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {

            try {
                while(rs.next()) {
                    s[num]=rs.getString(1);
                    num++;

                }
                statisticalList=new StatisticalList();
                if(s[0].equals("1")) {
                    statisticalList.setFasstatus("已关注");
                }

                if(s[1].equals("1")) {
                    statisticalList.setLikestatus("已点赞");
                }

                if(s[2].equals("1")) {
                    statisticalList.setCollectstatus("已收藏");
                }

              //  System.out.println(statisticalList.getFasstatus()+"   "+statisticalList.getLikestatus()+"    "+statisticalList.getCollectstatus());

                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return statisticalList;

    }






    /*
     *
     *
     *
     *
     *如果请求为点赞文章
     *如果请求为关注博主
     * * 如果请求收藏文章
     *
     *
     *
     */

    public int addmessage(String url,String articleid,String name,String othername,String title) {
        //设置修改sql语句
        String sql=null;
        if("/fans".equals(url)) {//关注

            sql="insert into fanslist(name,fans,time) values('"+name+"','"+othername+"',now())";
            //提交
        }
        if("/like".equals(url)) {//点赞
            sql="insert into likelist(name,thelike,time,articleid,title) values('"+name+"','"+othername+"',now(),'"+articleid+"','"+title+"')";

        }
        if("/collect".equals(url)) {//收藏
            sql="insert into collectlist(name,collector,articleid,time,title) values('"+name+"','"+othername+"','"+articleid+"',now(),'"+title+"')";

        }

        int n=jdbc.submitSQL(sql);
        if(!"/fans".equals(url)) {
            if(n==1) {

                String sql2=null;

                if("/like".equals(url)) {//点赞
                    sql2="update article set likenumber=likenumber+1  where id='"+articleid+"'";
                }
                if("/collect".equals(url)) {//收藏
                    sql2="update article set collect=collect+1 where id='"+articleid+"'";
                }
                n=jdbc.submitSQL(sql2);						    //提交

            }
        }
        return n;
    }

    /*
     *
     *
     *
     *如果请求为取消点赞文章
     *如果请求为取消关注博主
     * * 如果请求取消收藏文章
     *
     *
     */

    public int deletemessage(String url,String articleid,String name,String othername) {
        //设置修改sql语句
        String sql=null;
        if("/nofans".equals(url)) {//取消关注

            sql="delete from fanslist where name='"+name+"' and fans='"+othername+"'";//设置删除sql语句";
            //提交
        }
        if("/nolike".equals(url)) {//取消点赞
            sql="delete from likelist where name='"+name+"' and thelike='"+othername+"' and articleid='"+articleid+"'";//设置删除sql语句";

        }
        if("/nocollect".equals(url)) {//取消收藏
            sql="delete from collectlist where name='"+name+"' and collector='"+othername+"' and articleid='"+articleid+"'";//设置删除sql语句";

        }

        int n=jdbc.submitSQL(sql);
        System.out.println("sql="+n);
        if(!"/nofans".equals(url)) {
            if(n==1) {

                String sql2=null;

                if("/nolike".equals(url)) {//点赞
                    sql2="update article set likenumber=likenumber-1  where id='"+articleid+"'";
                }
                if("/nocollect".equals(url)) {//收藏
                    sql2="update article set collect=collect-1 where id='"+articleid+"'";
                }
                n=jdbc.submitSQL(sql2);						    //提交

            }
        }
        return n;
    }
    /*
     *
     * 添加文章访问量
     * 添加用户文章浏览历史
     *
     */
    public void addarticleviews(String id,String name,Article article) {//添加文章访问量

        String addarticleviews="update  article set pageview=pageview+1  where id='"+id+"'";//更新文章浏览量
        int n=jdbc.submitSQL(addarticleviews);	//执行更新语句

        if(n==1) {
            System.out.println("文章访问量添加成功");

            if(name!=null) {//添加浏览历史

                String  sql="insert into historylists(name,articleid,title,author,content,articletime,pageview,likenumber,comment,collect,historytime) values('"+name+"','"+article.getId()+"','"+article.getTitle()+"','"+article.getName()+"','"+article.getContent()+"','"+article.getTime()+"','"+article.getPageview()+"','"+article.getLikenumber()+"','"+article.getComment()+"','"+article.getCollect()+"',now())";
                int n2=jdbc.submitSQL(sql);
                if(n2==1) {
                    System.out.println("添加浏览历史成功");
                }
            }

        }
        else {
            System.out.println("文章访问量添加失败");
        }


    }


    /*
     *
     *
     * 添加网站访问量
     *
     */
    public void addpageviews(String address) {//添加网站访问量
        String sql=null;
        sql="insert into pageviews(time,address) values(now(),'"+address+"')";
        int n=jdbc.submitSQL(sql);
        if(n==1) {
            System.out.println("网页访问量添加成功");
        }
        else {
            System.out.println("网页访问量添加失败");
        }

    }


    /*
     *
     * 搜索文章
     *
     */
    public List<Article> searcharticle(int n,String content) {

        List<Article> list=new ArrayList<Article>();
        Article article=null;
        String sql=null;
        int number=5*(n-1);
        System.out.println("number为"+number);

        sql="select * from article where modality='公开' and status='通过审核'  and (name like '%"+content+"%'  or articlelabel like '%"+content+"%'  or content like '%"+content+"%' or title like '%"+content+"%') order by id desc LIMIT "+number+",5 ";//查询最近的前一百条文章
        System.out.println("sql"+sql);

        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {
            System.out.println("查询成功");
            try {
                while(rs.next()) {
                    article=new Article();
                    article.setId(rs.getInt("id"));
                    article.setName(rs.getString("name"));
                    article.setPhonenumber(rs.getString("phonenumber"));
                    article.setTitle(rs.getString("title"));
                    article.setContent(rs.getString("content"));
                    article.setArticlelabel(rs.getString("articlelabel"));
                    article.setSubfield(rs.getString("subfield"));
                    article.setArticletype(rs.getString("articletype"));
                    article.setModality(rs.getString("modality"));
                    article.setTime(rs.getString("time"));
                    article.setPageview(rs.getInt("pageview"));
                    article.setLikenumber(rs.getInt("likenumber"));
                    article.setComment(rs.getInt("comment"));
                    article.setCollect(rs.getInt("collect"));
                    list.add(article);

                }
                System.out.println(111111);
                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;


    }
/*
 *查询用户（被人关注，点赞，评论,收藏）信息提示
  *
  * */
public  List<String> news(String name) {
    String sql = "select count(*) from collectlist where status='未读' and name='"+name+"' and collector !='"+name+"' union all select count(*) from fanslist where status='未读' and name='"+name+"' and fans !='"+name+"' union all select count(*) from articlecomment where status='未读' and author='"+name+"' and name!='"+name+"' union all select count(*) from likelist where status='未读' and name='"+name+"' and thelike !='"+name+"' ";
    ResultSet rs = jdbc.queryDB(sql);
    List<String> list=new ArrayList<String>();
    int num = 0;
    if (rs != null) {
        try {
            while (rs.next()) {
              num+=rs.getInt(1);
              list.add(rs.getString(1));
            }
            list.add(Integer.toString(num));
            rs.close();                                 //关闭结果集
            jdbc.ps.close();                          //关闭PreparedStatement
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }
    else {
        for(int i=0;i<5;i++){
            list.add(Integer.toString(num));
        }
    }

        return list;
}



    /*
     *查询用户（被人关注，点赞，评论,收藏）的信息
     *
     * */
        public List<MessageList> newsList(String name,String check,int num) {
            List<MessageList> list=new ArrayList<MessageList>();
           MessageList messageList=null;
            String sql=null;
            String sql2=null;
            int number=5*(num-1);
            System.out.println("读取信息0:"+name+" num: "+num);
            if(check.equals("news")) {
                sql = "select * from articlecomment where author='"+name+"' and name !='"+name+"' order by id desc LIMIT "+number+",5  ";
            }

            if(check.equals("/newsComment")) {
                sql = "select * from articlecomment where author='"+name+"' and name !='"+name+"' order by id desc LIMIT "+number+",5  ";
            }
            if(check.equals("/newsLike")) {
                sql = "select * from likelist where name='"+name+"' and thelike !='"+name+"' order by id desc LIMIT "+number+",5  ";
            }
            if(check.equals("/newsFans")) {
                sql = "select * from fanslist where name='"+name+"' and fans !='"+name+"' order by id desc LIMIT "+number+",5  ";
            }
            if(check.equals("/newsCollect")) {
                sql = "select * from collectlist where name='"+name+"' and collector !='"+name+"' order by id desc LIMIT "+number+",5  ";
            }
            ResultSet rs=jdbc.queryDB(sql);
            if(rs!=null) {
                try {
                    while(rs.next()) {

                        messageList =new MessageList();
                        System.out.println("读取信息1"+rs.getString("name"));
                        if(check.equals("news")||check.equals("/newsComment")) {
                            messageList.setCommentname(rs.getString("name"));
                          //  messageList.setName(rs.getString("author"));
                            messageList.setTitleId(rs.getString("titleid"));
                            messageList.setComment(rs.getString("comment"));
                            messageList.setTitle(rs.getString("title"));
                        }
                        if(check.equals("/newsLike")) {
                            messageList.setThelike(rs.getString("thelike"));
                         //   messageList.setName(rs.getString("name"));
                            messageList.setTitleId(rs.getString("articleid"));
                            messageList.setTitle(rs.getString("title"));
                        }
                        if(check.equals("/newsFans")) {
                         //   messageList.setName(rs.getString("name"));
                            messageList.setFans(rs.getString("fans"));

                        }
                        if(check.equals("/newsCollect")) {
                            //messageList.setName(rs.getString("name"));
                            messageList.setCollector(rs.getString("collector"));
                            messageList.setTitle(rs.getString("title"));
                            messageList.setTitleId(rs.getString("articleid"));
                        }
                        messageList.setName(name);
                        messageList.setTime(rs.getString("time"));
                        messageList.setStatus(rs.getString("status"));

                      list.add(messageList);
                    }
                    rs.close();							     //关闭结果集
                    jdbc.ps.close();                          //关闭PreparedStatement
                    if(check.equals("/newsComment")) {
                        sql2 = "update articlecomment set status='已读' where author='" + name + "' and status='未读'";
                        int n=jdbc.submitSQL(sql2);
                    }
                    if(check.equals("news")) {
                        sql2 = "update articlecomment set status='已读' where author='" + name + "' and status='未读'";
                        int n=jdbc.submitSQL(sql2);
                    }
                    if(check.equals("/newsLike")) {
                        sql2 = "update likelist set status='已读' where name='" + name + "' and status='未读'";
                        int n=jdbc.submitSQL(sql2);
                    }
                    if(check.equals("/newsFans")) {
                        sql2 = "update fanslist set status='已读' where name='" + name + "' and status='未读'";
                        int n=jdbc.submitSQL(sql2);
                    }
                    if(check.equals("/newsCollect")) {
                        sql2 = "update collectlist set status='已读' where name='" + name + "' and status='未读'";
                        int n=jdbc.submitSQL(sql2);
                    }

                 				    //提交
                    System.out.println("测试。。。。。。。。");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }


            return list;
        }

/*
*
* 保存用户上传的文件信息
*
*
*/

        public boolean upload(UserDocument userDocument){
            String sql=null;
            System.out.println("userDocument"+userDocument);
            String path=userDocument.getPath().replaceAll("\\\\","/");//把路径中的斜杆替换成反斜杆，否则数据库会把斜杆自动去除
            System.out.println(path);
            sql="insert into document(name,filename,anotherfilename,path,time) values('"+userDocument.getName()+"','"+userDocument.getFilename()+"','"+userDocument.getAnotherfilename()+"','"+path+"',now())";
            int n=jdbc.submitSQL(sql);
            if(n==1) {
                System.out.println("文件信息保存成功");
                return true;
            }
            else {
                System.out.println("文件保存失败");
                return false;
            }

        }

    /*
     *
     *查询用户上传的文件信息
     *
     */
    public List<UserDocument> download(String username) {
        List<UserDocument> list=new ArrayList<UserDocument>();
       UserDocument userDocument=null;
        String sql="select * from document  where name='"+username+"' order by id desc ";
        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {

            try {
                while(rs.next()) {
                    userDocument=new UserDocument();
                    userDocument.setId(rs.getInt("id"));
                    userDocument.setName(rs.getString("name"));
                    userDocument.setFilename(rs.getString("filename"));
                    userDocument.setAnotherfilename(rs.getString("anotherfilename"));
                    userDocument.setPath(rs.getString("path"));
                    userDocument.setTime(rs.getString("time"));
                    list.add(userDocument);

                }
                System.out.println(userDocument);
                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return list;
    }
    /*
     *
     * 删除用户的文件信息
     *
     *
     */

    public boolean delete(String id){
        String sql=null;
         sql="delete from document where id='"+id+"'";
        int n=jdbc.submitSQL(sql);
        if(n==1) {
            System.out.println("文件信息删除成功");
            return true;
        }
        else {
            System.out.println("文件保删除失败");
            return false;
        }

    }




    /*
    *
    *
    * 查询用户的粉丝，关注的人，收藏的文章
    *
    * */
    public List<MessageList> look(String username,String check) {
        List<MessageList> list=new ArrayList<MessageList>();
        MessageList messageList=null;
        String sql=null;
        if(check.equals("查看粉丝")){
            sql="select * from fanslist  where name='"+username+"' order by id desc ";
        }
        if(check.equals("查看关注")){
            sql="select * from fanslist  where fans='"+username+"' order by id desc ";
        }
        if(check.equals("查看收藏")){
            sql="select * from collectlist  where collector='"+username+"' order by id desc ";
        }

        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {

            try {
                while(rs.next()) {
                    messageList=new MessageList();
                    messageList.setTime(rs.getString("time"));
                    if(check.equals("查看粉丝")) {
                        messageList.setFans(rs.getString("fans"));

                    }
                    if(check.equals("查看关注")){
                        messageList.setName(rs.getString("name"));
                    }
                    if(check.equals("查看收藏")){
                        messageList.setName(rs.getString("name"));
                        messageList.setTitle(rs.getString("title"));
                        messageList.setTitleId(rs.getString("articleid"));
                    }
                    list.add(messageList);
                    //System.out.println(messageList);

                }

                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return list;
    }

    /*
     *
     * 用户上传头像
     *
     *
     */

    public boolean updatepicture(String url,String name){
        String sql=null;
        sql="update user set picture='"+url+"' where name='"+name+"'";
        int n=jdbc.submitSQL(sql);
        if(n==1) {
            System.out.println("头像上传成功");
            return true;
        }
        else {
            System.out.println("头像上传失败失败");
            return false;
        }

    }

    /*
    *
    * 绑定用户邮箱
    *
    */
    public boolean bindingEmail(String mailbox,String name){
        String sql=null;
        sql="update user set mailbox='"+mailbox+"' where name='"+name+"'";
        int n=jdbc.submitSQL(sql);
        if(n==1) {
            System.out.println("邮箱绑定成功");
            return true;
        }
        else {
            System.out.println("邮箱绑定失败");
            return false;
        }

    }

    /*
     *
     * 用户修改密码
     *
     */
    public boolean udatepassword(String password,String name){
        String sql=null;
        String password2=password+"cjx1";//防止sql注入
        sql="update user set password='"+password2+"' where name='"+name+"'";
        int n=jdbc.submitSQL(sql);
        if(n==1) {
            System.out.println("密码修改成功");
            return true;
        }
        else {
            System.out.println("密码修改失败");
            return false;
        }

    }
   /*
   *
   * 检查邮箱是否为绑定的邮箱
   *
    */
    public boolean checkemail(String username,String email) {

        boolean check=false;
        String sql="select * from user  where name='"+username+"'and mailbox='"+email+"' ";
        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {

            try {
                while(rs.next()) {
                    check=true;
                System.out.println("checkemail");
                }

                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return check;
    }



    /*
    *
    *
    *
    * 查询用户的浏览历史
    *
    *
    *
    * */
    public List<Article> historyarticle(String name,int n) {
        int number=5*(n-1);
        String sql=null;
        if(n>1) {
            sql = "select*from historylists where name='" + name + "' order by id desc LIMIT " +number+",5 ";
        }
        else {
            sql = "select*from historylists where name='" + name + "'  order by id desc LIMIT 5";
        }
        Article article=null;
        List<Article> list=new ArrayList<Article>();
        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {

            try {
                while(rs.next()) {
                    article=new Article();
                    article.setTitle(rs.getString("title"));
                    article.setContent(rs.getString("content"));
                    article.setTime(rs.getString("articletime"));
                    article.setPageview(rs.getInt("pageview"));
                    article.setId(rs.getInt("articleid"));
                    article.setName(rs.getString("author"));
                    article.setHistorytime(rs.getString("historytime"));
                    list.add(article);
                }

                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;

    }


    /*
     *
     *用户删除文章
     *
     */
    public boolean deleteArticle(String name,String articleid){

        String sql=null;
        sql="delete from article where name='"+name+"' and id='"+articleid+"'";
        int n=jdbc.submitSQL(sql);
        if(n==1) {
            sql="delete from articlecomment where  titleid='"+articleid+"'";
            jdbc.submitSQL(sql);
            sql="delete from likelist where  articleid='"+articleid+"'";
            jdbc.submitSQL(sql);
            sql="delete from collectlist where  articleid='"+articleid+"'";
            jdbc.submitSQL(sql);
            System.out.println("文章删除成功");
            return true;
        }
        else {
            System.out.println("文章删除失败");
            return false;
        }

    }

    /*
    *
    * 删除历史
    *
    * */
   public boolean deletehistory(String name){
       String sql=null;
       sql="delete from historylists where name='"+name+"' ";
       int n=jdbc.submitSQL(sql);
       System.out.println("n==============="+n);
       if(n>=1){
           System.out.println("历史删除成功");
           return true;
       }
       else {
           System.out.println("历史删除失败");
           return false;
       }
   }


   /*
   *
   * 站点文章列表
   *
   *
   * */

    public List<SiteArticles> querySiteArticles(int n) {
        int number=5*(n-1);
        String sql=null;
        sql = "select*from sitearticles  order by id desc LIMIT "+number+",5 ";

        SiteArticles article=null;
        List<SiteArticles> list=new ArrayList<SiteArticles>();
        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {
            try {
                while(rs.next()) {
                    article=new SiteArticles();
                    article.setTitle(rs.getString("title"));
                    article.setContent(rs.getString("content"));
                    article.setPageview(rs.getInt("pageview"));
                    article.setPicture(rs.getString("picture"));
                    article.setId(rs.getInt("id"));
                    article.setName(rs.getString("name"));
                    article.setTime(rs.getString("time"));
                    list.add(article);
                }

                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;

    }


    /*
     *
     * 查看站点文章
     *
     *
     * */

    public SiteArticles querySiteArticles2(int id) {

        String sql=null;
        sql = "select*from sitearticles  where id='"+id+"'";

        SiteArticles article=null;
        ResultSet rs=jdbc.queryDB(sql);
        if(rs!=null) {
            try {
                while(rs.next()) {
                    article=new SiteArticles();
                    article.setTitle(rs.getString("title"));
                    article.setContent(rs.getString("content"));
                    article.setPageview(rs.getInt("pageview"));
                    article.setPicture(rs.getString("picture"));
                    article.setId(rs.getInt("id"));
                    article.setName(rs.getString("name"));
                    article.setTime(rs.getString("time"));

                }

                rs.close();							     //关闭结果集
                jdbc.ps.close();                          //关闭PreparedStatement
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return article;

    }



 /*
 *
 * 添加站点文章访问量
 *
 * */
    public void addSiteArticlesPageView(String id) {//添加网站访问量
        String sql=null;
        sql="update sitearticles set pageview=pageview+1 where id='"+id+"'";
        int n=jdbc.submitSQL(sql);
        if(n==1) {
            System.out.println("站点文章访问量添加成功");
        }
        else {
            System.out.println("站点文章问量添加失败");
        }

    }



}
