package com.dao.entiy;

/*
**用户上传文件的信息
*/

public class UserDocument {
    private int id;//文件id
    private String name;//用户昵称
    private String filename;//文件的原来名称
    private String anotherfilename;//文件上传时的名称
    private String path;//文件上传路径
    private String time;//文件上传时间


    public UserDocument() {

    }

    public UserDocument(String name, String filename, String anotherfilename, String path) {

        this.name = name;
        this.filename = filename;
        this.anotherfilename = anotherfilename;
        this.path = path;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getAnotherfilename() {
        return anotherfilename;
    }

    public void setAnotherfilename(String anotherfilename) {
        this.anotherfilename = anotherfilename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "UserDocument{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", filename='" + filename + '\'' +
                ", anotherfilename='" + anotherfilename + '\'' +
                ", path='" + path + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
