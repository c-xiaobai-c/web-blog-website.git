package com.dao.entiy;
/*
*用于封装用户的（被人关注，点赞，评论）的信息提示
*
*/

public class MessageList {
    private String fans;//粉丝
    private String collector;//收藏者
    private String commentname;//评论者
    private String thelike;//点赞者
    private String time;
    private String title;//文章标题
    private String titleId;//文章id
    private String name;//作者
    private String comment;//评论
    private String status;//状态

    public MessageList(){

    }

    @Override
    public String toString() {
        return "MessageList{" +
                "fans='" + fans + '\'' +
                ", collector='" + collector + '\'' +
                ", commentname='" + commentname + '\'' +
                ", thelike='" + thelike + '\'' +
                ", time='" + time + '\'' +
                ", title='" + title + '\'' +
                ", titleId='" + titleId + '\'' +
                ", name='" + name + '\'' +
                ", comment='" + comment + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public String getThelike() {
        return thelike;
    }

    public void setThelike(String thelike) {
        this.thelike = thelike;
    }

    public String getCommentname() {
        return commentname;
    }

    public void setCommentname(String commentname) {
        this.commentname = commentname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFans() {
        return fans;
    }

    public void setFans(String fans) {
        this.fans = fans;
    }

    public String getCollector() {
        return collector;
    }

    public void setCollector(String collector) {
        this.collector = collector;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
