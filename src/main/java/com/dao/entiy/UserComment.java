package com.dao.entiy;
/*
 *
 用于用户对文章的评论
 *
*/
public class UserComment {

    private int id;
    private String phonenumber;
    private String name;
    private String comment;
    private String title;
    private String titled;
    private String time;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getPhonenumber() {
        return phonenumber;
    }
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitled() {
        return titled;
    }
    public void setTitled(String titled) {
        this.titled = titled;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }


}
