package com.dao.entiy;
/*
用于发布文章
*/
public class Article {
    private int id;

    private String name;//用户昵称
    private String phonenumber;//用户手机号
    private String title;//文章标题
    private String content;//文章内容
    private String  articlelabel;//文章标签
    private String subfield;//分栏专类
    private String articletype;//文章类型
    private String modality;//发布形式
    private String time;//发布时间
    private int pageview;//浏览量
    private int  likenumber;//点赞数量
    private int comment;//评论数量
    private int collect;//收藏数量
    private String status;
    private String historytime;//浏览历史

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHistorytime() {
        return historytime;
    }

    public void setHistorytime(String historytime) {
        this.historytime = historytime;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPhonenumber() {
        return phonenumber;
    }
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getArticlelabel() {
        return articlelabel;
    }
    public void setArticlelabel(String articlelabel) {
        this.articlelabel = articlelabel;
    }
    public String getSubfield() {
        return subfield;
    }
    public void setSubfield(String subfield) {
        this.subfield = subfield;
    }
    public String getArticletype() {
        return articletype;
    }
    public void setArticletype(String articletype) {
        this.articletype = articletype;
    }
    public String getModality() {
        return modality;
    }
    public void setModality(String modality) {
        this.modality = modality;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public int getPageview() {
        return pageview;
    }
    public void setPageview(int pageview) {
        this.pageview = pageview;
    }
    public int getLikenumber() {
        return likenumber;
    }
    public void setLikenumber(int likenumber) {
        this.likenumber = likenumber;
    }

    public int getComment() {
        return comment;
    }
    public void setComment(int comment) {
        this.comment = comment;
    }
    public int getCollect() {
        return collect;
    }
    public void setCollect(int collect) {
        this.collect = collect;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phonenumber='" + phonenumber + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", articlelabel='" + articlelabel + '\'' +
                ", subfield='" + subfield + '\'' +
                ", articletype='" + articletype + '\'' +
                ", modality='" + modality + '\'' +
                ", time='" + time + '\'' +
                ", pageview=" + pageview +
                ", likenumber=" + likenumber +
                ", comment=" + comment +
                ", collect=" + collect +
                ", status='" + status + '\'' +
                ", historytime='" + historytime + '\'' +
                '}';
    }
}
