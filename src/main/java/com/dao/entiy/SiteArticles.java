package com.dao.entiy;


/*
*
* 站点文章实体类
* */
public class SiteArticles {

    private int id;
    private String name;
    private String title;
    private String picture;
    private String content;
    private String time;
    private int pageview;

    public SiteArticles() {

    }

    public SiteArticles(int id, String name, String title, String picture, String content, String time, int pageview) {
        this.id = id;
        this.name = name;
        this.title = title;
        this.picture = picture;
        this.content = content;
        this.time = time;
        this.pageview = pageview;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getPageview() {
        return pageview;
    }

    public void setPageview(int pageview) {
        this.pageview = pageview;
    }
}
