package com.dao.entiy;
/*
 *
 * 用于统计文章的各项数量
 *
 *
 */
public class StatisticalList {

    private int fanssum;//粉丝数量
    private int likesum;//点赞数量
    private int pageviewsum;//浏览数量
    private int collectsum;//收藏数量
    private int commentsum;//评论数量
    private int articlesum;//文章总量
    private String name;//用户
    private String picture;//用户头像
    private String collectstatus;//判断是否收藏了
    private String likestatus;//判断是否点赞了
    private String fasstatus;//判断是否收藏了

    public int getArticlesum() {
        return articlesum;
    }

    public void setArticlesum(int articlesum) {
        this.articlesum = articlesum;
    }

    public int getFanssum() {
        return fanssum;
    }
    public void setFanssum(int fanssum) {
        this.fanssum = fanssum;
    }
    public int getLikesum() {
        return likesum;
    }
    public void setLikesum(int likesum) {
        this.likesum = likesum;
    }
    public int getPageviewsum() {
        return pageviewsum;
    }
    public void setPageviewsum(int pageviewsum) {
        this.pageviewsum = pageviewsum;
    }
    public int getCollectsum() {
        return collectsum;
    }
    public void setCollectsum(int collectsum) {
        this.collectsum = collectsum;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPicture() {
        return picture;
    }
    public void setPicture(String picture) {
        this.picture = picture;
    }
    public int getCommentsum() {
        return commentsum;
    }
    public void setCommentsum(int commentsum) {
        this.commentsum = commentsum;
    }
    public String getCollectstatus() {
        return collectstatus;
    }
    public void setCollectstatus(String collectstatus) {
        this.collectstatus = collectstatus;
    }
    public String getLikestatus() {
        return likestatus;
    }
    public void setLikestatus(String likestatus) {
        this.likestatus = likestatus;
    }
    public String getFasstatus() {
        return fasstatus;
    }
    public void setFasstatus(String fasstatus) {
        this.fasstatus = fasstatus;
    }



}
