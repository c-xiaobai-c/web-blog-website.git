package com.dao;

import java.io.PrintWriter;

import com.alibaba.fastjson.JSON;

/*
用于转换json,把数据转换为json字符串提交给前端，或把json字符串遍历出每个值存于数据库
*/
public class JsonConversion {

    public JsonConversion() {

    }
    public String json(Object object) {


        String jsonArray = JSON.toJSONString(object);//转换为json字符串
        // System.out.println("json数据"+jsonArray);

        return jsonArray;


    }


}
