package com.dao;

import com.dao.Impl.WangEditor;
import com.dao.entiy.User;
import com.dao.entiy.UserDocument;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;

/*
*
*
* 文件上传功能
*
* */
public class Upload {
    private HttpServletResponse response;
    private HttpServletRequest request;

    public Upload() {

    }


    /*
    * 上传图片（用户头像）到服务器
    *
    * */
    public boolean headSculpture(Part part, HttpServletRequest request) throws IOException, ServletException {

        if (part.getContentType().contains("image")) {//判断是否为图片

            if(upload(part,request,"上传头像")){

                return  true;
            }
            else {
                return false;
            }

        }
        else{
            return false;
        }


    }


  /*
  *
  *上传文件到服务器
  *
    */
    public boolean upload(Part part, HttpServletRequest request,String username) throws IOException, ServletException {
       boolean bool=false;
        String disposition = part.getHeader("Content-Disposition");
        System.out.println("数据名为"+disposition);
        String name = null;//原来的文件名
        try {
            name  = disposition.substring(disposition.lastIndexOf("\"",disposition.lastIndexOf("\"")-1)+1,disposition.length()-1);//上传的名字
          
            System.out.println("文件名为"+name);
            System.out.println("\"");
        } catch (Exception e) {
            // TODO: handle exception
        }
        if (name.equals("")){
          bool=false;

        }
        else {
            String suffix = disposition.substring(disposition.lastIndexOf("."), disposition.length() - 1);
            //随机的生存一个32的字符串
            System.out.println(suffix);
            String filename = UUID.randomUUID() + suffix;
            //获取上传的文件名
            System.out.println(filename);
            InputStream is = part.getInputStream();
            //动态获取服务器的路径
            String serverpath = request.getServletContext().getRealPath("upload");
            //如果路径不存在，创建一个
            File realPath = new File(serverpath);
            if (!realPath.exists()) {
                realPath.mkdir();
            }
            System.out.println("服务器的路径" + serverpath);
            FileOutputStream fos = new FileOutputStream(serverpath + "/" + filename);
            byte[] bty = new byte[1024];
            int length = 0;
            int i = 0;
            while ((length = is.read(bty)) != -1) {
                fos.write(bty, 0, length);
                //  System.out.println(++i);
            }
            fos.close();
            is.close();

            WangEditor wangEditor = new WangEditor();
            if(username.equals("上传头像")){
                List<User> user = (List<User>) request.getSession().getAttribute("user");//获取保存在会话中的用户账号
                User user1 = user.get(0);
                bool=wangEditor.updatepicture("/upload/"+filename,user1.getName());
            }
            else {
                UserDocument userDocument = new UserDocument(username, name, filename, serverpath);//保存文件信息到数据库
                bool = wangEditor.upload(userDocument);
            }

        }
        return bool;
    }

    /*
    *
    *下载文件
     *
     */
    public boolean downloads(HttpServletRequest request, HttpServletResponse response,String filename) throws Exception{
        this.response = response;
        this.request = request;
        //要下载的图片地址
        String  path = request.getServletContext().getRealPath("/upload");
        String  fileName = filename;

        //1、设置response 响应头
        response.reset(); //设置页面不缓存,清空buffer
        response.setCharacterEncoding("UTF-8"); //字符编码
        response.setContentType("multipart/form-data"); //二进制传输数据
        //设置响应头
        response.setHeader("Content-Disposition", "attachment;fileName="+ URLEncoder.encode(fileName, "UTF-8"));

        File file = new File(path,fileName);
        //2、 读取文件--输入流
        InputStream input=new FileInputStream(file);
        //3、 写出文件--输出流
        OutputStream out = response.getOutputStream();

        byte[] buff =new byte[1024];
        int index=0;
        //4、执行 写出操作
        while((index= input.read(buff))!= -1){
            out.write(buff, 0, index);
            out.flush();
        }
        out.close();
        input.close();
        return true;
    }

    /*
    *
    *删除上传的文件
    *
    */
    public boolean deleteFile(HttpServletRequest request,String filename,String id){

        String  path = request.getServletContext().getRealPath("/upload");
        System.out.println("path"+path);
        System.out.println("filename"+filename);
        File file=new File(path+"/"+filename);//创建文件对象
        if(file.exists()) {//如果文件存在
            file.delete();//删除文件
            System.out.println("文件已删除");
            WangEditor wangEditor=new WangEditor();
            return wangEditor.delete(id);
        }
        else{
            return  false;
        }
    }

}
