package com.dao;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

//import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.Impl.WangEditor;
import com.dao.entiy.Article;
import com.dao.entiy.StatisticalList;
import com.dao.entiy.User;

public class FunctionalProcessing {
    WangEditor wangEditor=new WangEditor();
    public FunctionalProcessing() {

    }

    public StatisticalList usermessage(List<Article> article) {//查询博主的总粉丝数量，总被点赞数量，总被收藏数量，总访问数量
        List<Article>  list=article;
        Article article2=list.get(0);
        //   WangEditor wangEditor=new WangEditor();
        StatisticalList statisticalList=wangEditor.articleperson(article2.getName());
        return statisticalList;


    }
    public StatisticalList messagelist(List<Article> article,List <User> user) {//查询该文章是否被点赞，收藏
        List<Article>  list=article;
        StatisticalList statisticalList=null;
        Article article2=null;
        User user2=null;
        try {
            article2=list.get(0);
            List<User>   user1=user;
            System.out.println("test:"+user1);
            if(user1!=null) {
                user2=user1.get(0);
                //	 WangEditor wangEditor=new WangEditor();
                statisticalList=wangEditor.fans(article2.getName(),user2.getName(),article2.getId());
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }



        return statisticalList;


    }
    /*
     *
     *
     * 查询作者的五篇最新的文章
     */
    public List<Article> newarticle(String name,int n) {


        return  wangEditor.newarticle(name,n,null);
    }





    /*
     * 如果请求为点赞文章
     *如果请求为关注博主
     * 如果请求收藏文章
     *
     *
     */
    public int addmessage(String url,String articleid,String name,List <User> othername,String title) {
        int n=0;
        String othername1=othername.get(0).getName();
        System.out.println(url+":"+articleid+":"+name+":"+othername1);
        // WangEditor wangEditor=new WangEditor();
        n=wangEditor.addmessage(url, articleid, name, othername1,title);



        return n;


    }
    /*
     * 如果请求为取消点赞文章
     *如果请求为取消关注博主
     * 如果请求取消收藏文章
     *
     *
     */
    public int deletemessage(String url,String articleid,String name,List <User> othername) {
        int n=0;
        String othername1=othername.get(0).getName();
        System.out.println(url+":"+articleid+":"+name+":"+othername1);
        //WangEditor wangEditor=new WangEditor();
        n=wangEditor.deletemessage(url, articleid, name, othername1);



        return n;


    }


    /*
     *
     *添加文章访问量，浏览历史
     *
     */
    public void addarticleviews(String id,List <User> user, List <Article> personarticle) {//添加文章访问量
        WangEditor wangEditor=new WangEditor();
        List<User>   user1=user;
        String name = null;
        try{
            List <Article> articles=personarticle;
            Article article=articles.get(0);
            if(user1!=null) {
                User	user2=user1.get(0);
                name=user2.getName();
            }
            wangEditor.addarticleviews(id,name,article);
        }catch (Exception e){

        }



    }

    /*
     *
     *
     *添加网站访问量
     *
     */
    public void addpageviews(String address) {//添加网站访问量
        WangEditor wangEditor=new WangEditor();
        wangEditor.addpageviews(address);


    }
/*
*
* 分页查询
*
*/

    public List<Article> searcharticle(String content) {

        WangEditor wangEditor=new WangEditor();

        return wangEditor.searcharticle(1, content);


    }

    public String searcharticle(String content,int n) {//分页查询

        WangEditor wangEditor=new WangEditor();
        JsonConversion jsonConversion=new JsonConversion();
        String jsonArry=jsonConversion.json(wangEditor.searcharticle(n, content));

        return jsonArry;


    }
    /*

    防止用户频繁评论，限制十五分钟内只能对同一文章评论一次
    */
    public String check(String id,List<User>users,HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        String s=null;
        User	user2=users.get(0);
        String username=user2.getPhonenumber()+id;
        Cookie[] cookies=request.getCookies();
        System.out.println("["+users+"]");
        if(cookies !=null&&cookies.length>0) {
            for(Cookie cookie:cookies) {
                if(cookie.getName().equals(username)) {
                    System.out.println("name为："+cookie.getName());
                    System.out.println("cookie为："+  URLDecoder.decode(cookie.getValue(), "utf-8"));
                    request.setAttribute("comment", "comment");
                    s="false";
                }

            }
        }

        if(request.getAttribute("comment")==null) {

            Cookie comment = new Cookie(username,URLEncoder.encode("评论","utf-8")); // 中文转码

            //  Cookie 设置过期日期为 15分钟后
            comment.setMaxAge(60*15);
            response.addCookie(comment);
            System.out.println("comment不存在");
            s="accept";

        }
        return s;

    }





}
