package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.dao.*;
import com.dao.Impl.Impl;
import com.dao.Impl.WangEditor;
import com.dao.entiy.*;
import com.dao.utils.JDBCMySql;


/**
 *
 *
 *
 */
@WebServlet("*.do")
@MultipartConfig  //使用MultipartConfig注解标注改servlet能够接受文件上传
//@WebServlet("/register")
public class MyServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private JDBCMySql jdbc=JDBCMySql.getJDBC();		   						//调用封装好的JDBC
    private Impl impl=new Impl();								//调用业务类
    private WangEditor wangEditor=new WangEditor();
    private 	IndentifyingCode1 indentifyingCode1=new IndentifyingCode1();//发送验证码函数
    @Override
    public void init() throws ServletException {
        System.out.println("serlvet被创建了！");
        jdbc.connectDB();    //初始化时连接数据库

    }




    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out=response.getWriter();

        String uri=request.getRequestURI();//获取获取页面请求路径
        String action=uri.substring(uri.lastIndexOf("/"), uri.lastIndexOf("."));
        System.out.println("post:"+action);



        String p1=request.getParameter("phonenumber");	 			//获取手机号参数
        String p2=request.getParameter("password");			//获取密码参数
        String name=request.getParameter("name");	       //获取用户昵称



        Cookie[] cookies=request.getCookies();
        if(cookies !=null&&cookies.length>0) {
            for(Cookie cookie:cookies) {
                if(cookie.getName().equals("view")) {
                    System.out.println("name为："+cookie.getName());
                    System.out.println("cookie为："+  URLDecoder.decode(cookie.getValue(), "utf-8"));
                    request.setAttribute("view", "view");
                }

            }
        }

        if(request.getAttribute("view")==null) {

            Cookie view = new Cookie("view",URLEncoder.encode("访问","utf-8")); // 中文转码

            //  Cookie 设置过期日期为 30分钟后
            view.setMaxAge(60*60*12);
            response.addCookie(view);
            System.out.println("cookie不存在");
            FunctionalProcessing functionalProcessing=new FunctionalProcessing();
            functionalProcessing.addpageviews(request.getParameter("address"));//添加网站访问数量

        }



        if("/code".equals(action)) {//如果请求发送验证码
            if(p1!=null) {
                String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(16[5,6])|(17[0-8])|(18[0-9])|(19[1、5、8、9]))\\d{8}$";//手机号码正则
                Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(p1);
                if (!m.matches()) {
                    out.write("请输入正确的手机号");
                } else {
                    if (impl.query(p1).equals("账号不存在")) {

                        String verifyCode = String.valueOf(new Random().nextInt(89999) + 10000);//生成5位验证码
                        if (indentifyingCode1.code(p1, verifyCode)) {//如果验证码已发送

                            Date date = new Date();//定义发送验证码的时间，用于验证验证码是否过期
                            request.getSession().setAttribute("code", date);//保存该时间
                            request.getSession().setAttribute("code2", verifyCode);//保存该验证码
                            out.write("发送成功");//向前端返回数据
                        } else {
                            out.write("发送失败！");//向前端返回数据
                        }


                    } else {
                        out.write("该手机号已注册！");//向前端返回数据
                    }
                }
            }
            else{
                out.write("手机号不能为空");
            }

        }
        if("/register".equals(action)){//如果请求为注册

            //获取前端传递数据
            //	String p1=request.getParameter("phonenumber");	 			//获取手机号参数
            //	String p2=request.getParameter("password");			//获取密码参数
            String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(16[5,6])|(17[0-8])|(18[0-9])|(19[1、5、8、9]))\\d{8}$";//手机号码正则
            // String reg_password= "^(?![^a-zA-Z]+$)(?!\\D+$)[a-zA-Z0-9!@#$%]{8,15}$";  //密码正则
            Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(p1);
            String regStr="^(?![a-zA-z]+$)(?!\\d+$)(?![!@#$%^&*]+$)[a-zA-Z\\d!@#$%^&*]+$";    //密码(必须包括字母和数字,字符长度9--15之间)
            String re_name="^[\u4e00-\u9fa5]{0,10}$";
            //没有正则过滤就被sql注入了，（或者可以使用md5加密）
            if(p1!=null&&p2!=null&&name!=null) {
                if (!m.matches()) {
                    out.write("请输入正确的手机号");
                } else if (!p2.matches(regStr)) {
                    out.write("请输入正确的密码格式");
                }
                else if(!name.matches(re_name)){
                    out.write("请输入正确的昵称格式");
                }
                else {
                   if(request.getSession().getAttribute("code2")!=null&&request.getParameter("code")!=null) {

                       if (impl.query(p1).equals("账号已存在")) {
                           out.write("该手机号已被注册，请勿重新注册！");//向前端返回数据

                       } else if (impl.query2(name).equals("昵称已存在")) {
                           out.write("该昵称已被注册，请重新填写！");//向前端返回数据

                       } else if (impl.query(p1).equals("账号不存在") && impl.query2(name).equals("昵称不存在")) {
                           Date date = (Date) request.getSession().getAttribute("code");//获取发送验证码时的时间
                           String p3 = request.getParameter("code");            //获取验证码参数
                           if (indentifyingCode1.timer(date) < 120 && request.getSession().getAttribute("code2").equals(p3)) {
                               Integer n = impl.add(p1, p2, name);                         //向数据库中保存数据

                               out.write("注册成功！");//向前端返回数据
                           } else {
                               if (!request.getSession().getAttribute("code2").equals(p3)) {
                                   out.write("验证码错误，请重新填或发送！");//向前端返回数据
                                   System.out.println(request.getSession().getAttribute("code2"));
                                   System.out.println(p3);
                               } else {
                                   out.write("验证码过期，请重新发送！");//向前端返回数据
                               }
                           }

                       }
                   }
                   else{
                       out.write("请获取验证码！");//向前端返回数据
                   }
                }
                }
            else{
                    out.write("账号或密码或昵称不能为空");//向前端返回数据
                }

        }

        if("/login".equals(action)) {//如果请求为登录
            String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(16[5,6])|(17[0-8])|(18[0-9])|(19[1、5、8、9]))\\d{8}$";//手机号码正则
          // String reg_password= "^(?![^a-zA-Z]+$)(?!\\D+$)[a-zA-Z0-9!@#$%]{8,15}$";  //密码正则
            Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(p1);
            String regStr="^(?![a-zA-z]+$)(?!\\d+$)(?![!@#$%^&*]+$)[a-zA-Z\\d!@#$%^&*]+$";    //密码(必须包括字母和数字,字符长度9--15之间)
            //没有正则过滤就被sql注入了，（或者可以使用md5加密）
            if(p1!=null&&p2!=null) {
                if (!m.matches()) {
                    out.write("请输入正确的手机号");
                } else if (!p2.matches(regStr)) {
                    out.write("请输入正确的密码格式");
                }
                else {
                    List<User> user = impl.login("登录", p1, p2);
                    //request.getSession().setAttribute("user",user);
                    if (user != null && !user.isEmpty()) {
                        out.write("登录成功！");//向前端返回数据
                        request.getSession().setAttribute("user", user);//保存用户的登录信息
                        // request.getRequestDispatcher("index.jsp").forward(request, response)
                        System.out.println("登录成功！");


                    } else {
                        out.write("登录失败！");//向前端返回数据
                        System.out.println("登录失败！");
                    }
                }

            }
            else{
                out.write("账号或密码不能为空");//向前端返回数据
            }

        }
        /*
        *
        * 查询刷新用户被关注，点赞，评论的消息
        */
        if(request.getSession().getAttribute("user")!=null){

            WangEditor wangEditor=new WangEditor();
            List<User> user=(List<User>)request.getSession().getAttribute("user");//获取保存在会话中的用户账号
           // String s=Integer.toString(wangEditor.news(user.get(0).getName()));//将未读信息保存到会话中
          List<String> list=wangEditor.news(user.get(0).getName());//将未读信息保存到会话中
            request.getSession().setAttribute("news",list);


        }

        if("/savearticle".equals(action)) {//如果请求为发布文章
            Article article=new Article();

           // System.out.println("内容过多，上传失败！:"+request.getParameter("title"));
          // System.out.println(request.getParameter("content"));
            String title=request.getParameter("title");
            System.out.println("content======"+request.getParameter("content"));
            List<User>   user=(List<User>)request.getSession().getAttribute("user");//获取保存在会话中的用户账号
            if(user!=null&&!user.isEmpty()) {


                if (request.getParameter("title") != null && request.getParameter("phonenumber") != null && request.getParameter("content") != null && request.getParameter("title") != null && request.getParameter("articletype") != null && request.getParameter("check") != null && request.getParameter("modality") != null) {
                    if (title == null) {//防止数据传输失败
                        out.write("内容过多，上传失败！");
                        System.out.println("内容过多，上传失败");
                    } else {
                        System.out.println("articleid===" + request.getParameter("articleid"));
                        System.out.println("edit" + request.getParameter("check"));
                        article.setName(request.getParameter("name"));
                        article.setPhonenumber(request.getParameter("phonenumber"));
                        article.setTitle(request.getParameter("title"));
                        article.setContent(request.getParameter("content"));
                        article.setArticlelabel(request.getParameter("articlelabel"));
                        article.setSubfield(request.getParameter("subfield"));
                        article.setArticletype(request.getParameter("articletype"));
                        article.setModality(request.getParameter("modality"));
                        try {
                            article.setId(Integer.parseInt(request.getParameter("articleid")));
                        } catch (Exception e) {

                        }
                        String check = request.getParameter("check");//判断是发布新文章，还是更新文章

                        if (wangEditor.add(article, check) == 1) {

                            out.write("发布成功！");
                            System.out.println("发布成功！");
                        } else {
                            out.write("发布失败！");
                            System.out.println("发布失败,图片过大");
                        }
                    }
                } else {
                    out.write("不能为空");
                }
            }
            else{
                out.write("请先登陆！");
            }

        }
        if("/index".equals(action)) {//如果请求为首次进入网页
            List <Article> article=wangEditor.query("article",null,1);
            request.getSession().setAttribute("article",article);
            if(article!=null&&!article.isEmpty()){
                out.write("查询成功！");
            }
            // JsonConversion jsonConversion=new JsonConversion();//调用json转换功能类
            //  String jsonArry=jsonConversion.json(wangEditor.query());
            //	request.getSession().setAttribute("articlejsonArry",jsonArry);

            // out.write(jsonArry);


        }
        if("/hotArticle".equals(action)) {//如果请求为查看热度文章


            JsonConversion jsonConversion=new JsonConversion();//调用json转换功能类
            String jsonArry=jsonConversion.json(wangEditor.query("hotArticle",null,Integer.parseInt(request.getParameter("n"))));
//		      request.getSession().setAttribute("articlejsonArry",jsonArry);
            if(jsonArry.equals("[]")) {
                out.write("失败了！");//返回json字符串
              //  System.out.println("hotarticlejsonarry1..........."+jsonArry);
            }
            else {
                out.write(jsonArry);//返回json字符串
               // System.out.println("hotarticlejsonarry2..........."+jsonArry);
            }

            // out.write(jsonArry);//返回json字符串

        }
        if("/newArticle".equals(action)) {//如果请求为查看最新文章


            JsonConversion jsonConversion=new JsonConversion();//调用json转换功能类
            System.out.println("检查。。"+request.getParameter("n"));
            String jsonArry=jsonConversion.json(wangEditor.query("newArticle",null,Integer.parseInt(request.getParameter("n"))));
            if(jsonArry.equals("[]")) {
                out.write("失败了！");
               // System.out.println("newarticlejsonarry1..........."+jsonArry);
            }
            else {
                out.write(jsonArry);//返回json字符串
               // System.out.println("newarticlejsonarry2..........."+jsonArry);
            }

        }
        /*
         *
         * 如果请求为评论文章
         */
        if("/comment".equals(action)) {//如果请求为评论文章
            System.out.println("评论");
            List<User>   user=(List<User>)request.getSession().getAttribute("user");//获取保存在会话中的用户账号


            //保存评论
            if(user!=null&&!user.isEmpty()){
                if( request.getParameter("comment")!=null && request.getParameter("titleid")!=null &&request.getParameter("title")!=null && request.getParameter("author")!=null) {


                    FunctionalProcessing functionalProcessing = new FunctionalProcessing();
                    if (functionalProcessing.check(request.getParameter("titleid"), user, request, response).equals("false")) {//限制用户频繁评论，十五分钟内只能对同意文章评论一次
                        out.write("请勿频繁评论！");
                        System.out.println("请勿频繁评论！");
                    } else {
                        if (wangEditor.addcomment(user.get(0).getPhonenumber(), user.get(0).getName(), request.getParameter("comment"), request.getParameter("titleid"), request.getParameter("title"), request.getParameter("author")) == 1) {

                            JsonConversion jsonConversion = new JsonConversion();//调用json转换功能类
                            String jsonArry = jsonConversion.json(wangEditor.coment(request.getParameter("titleid")));//文章评论内容

                            out.write(jsonArry);

                        } else {
                            out.write("评论失败！");
                        }
                    }

                } else{
                    out.write("不能为空");
                }

            }
            else{
                out.write("请先登陆！");
            }

        }
        /*
         *如果请求为点赞文章
         *如果请求为关注博主
         * 如果请求收藏文章
         *
         */
        if("/like".equals(action)||"/fans".equals(action)||"/collect".equals(action)) {

            List <User> user=(List<User>) request.getSession().getAttribute("user");
            if(user!=null&&!user.isEmpty()){
                if(request.getParameter("titleid")!=null&&request.getParameter("name")!=null&&request.getParameter("title")!=null) {
                    FunctionalProcessing fProcessing = new FunctionalProcessing();
                    if (fProcessing.addmessage(action, request.getParameter("titleid"), request.getParameter("name"), user, request.getParameter("title")) == 1) {

                        out.write("成功！");
                    } else {
                        out.write("失败！");
                    }

                }
                else{
                    out.write("不能为空");
                }


            }
            else {
                out.write("请先登陆！");
            }

        }
        /*
         *
         * 如果请求为取消点赞
         * 如果请求为取消关注博主
         * 如果请求取消收藏文章
         */
        if("/nolike".equals(action)||"/nofans".equals(action)||"/nocollect".equals(action)) {
            List <User> user=(List<User>) request.getSession().getAttribute("user");
            if(user!=null&&!user.isEmpty()){
                if(request.getParameter("titleid")!=null&&request.getParameter("name")!=null) {
                    FunctionalProcessing fProcessing = new FunctionalProcessing();
                    if (fProcessing.deletemessage(action, request.getParameter("titleid"), request.getParameter("name"), user) == 1) {

                        out.write("成功！");
                    } else {
                        out.write("失败！");
                    }
                }
                else{
                    out.write("不能为空！");
                }

            }
            else {
                out.write("请先登陆！");
            }

        }

        if("/searcharticle".equals(action)){//搜索查询文章
            //response.sendRedirect("login.jsp");

            FunctionalProcessing fProcessing=new FunctionalProcessing();
            String string=fProcessing.searcharticle(request.getParameter("keyboard"),Integer.parseInt(request.getParameter("n")));
            if(string.equals("[]")) {
                out.write("失败！");
                System.out.println("nothing");
            }
            else{

                out.write(string);

            }
            System.out.println("测试成功！！"+request.getParameter("keyboard"));

        }

        /*
        * 反馈发送邮箱
        * */
        if("/sendmail".equals(action)){//反馈发送邮箱


            if(request.getParameter("mailmessage")!=null) {

                SendMail sendMail = new SendMail();
                try {
                    out.write(sendMail.sendmail(request.getParameter("mailmessage"), request, response));//
                } catch (GeneralSecurityException e) {
                    // TODO 自动生成的 catch 块
                    e.printStackTrace();
                }
            }
            else{
                out.write("信息不能为空！");
            }



        }

/*
*
* 查询用户评论信息显示在消息页
*
* */
        if("/newsComment".equals(action)||"/newsLike".equals(action)||"/newsFans".equals(action)||"/newsCollect".equals(action)) {
            WangEditor wangEditor = new WangEditor();
            JsonConversion jsonConversion = new JsonConversion();
            System.out.println("加载更多信息1");
            String arrayJson = jsonConversion.json(wangEditor.newsList(request.getParameter("name"), action, Integer.parseInt(request.getParameter("num"))));
            System.out.println("加载更多信息2");
            if (arrayJson.equals("[]")) {
                out.write("没有数据了");

            }
            else{
                out.write(arrayJson);
            }
        }


        //请求上传资源到服务器
        if("/upload".equals(action)) {
            if (request.getSession().getAttribute("user") != null) {
                List<User> user = (List<User>) request.getSession().getAttribute("user");//获取保存在会话中的用户账号
                User user1 = user.get(0);
                if (user1.getLevel().equals("超级会员")) {

                    try{
                        Part part = request.getPart("file");//获取上传的资源属性名字
                        Upload up = new Upload();//调用上传类
                        if (up.upload(part, request,user1.getName())) {//如果返回结果为真
                            out.write("上传成功");
                        } else {
                            out.write("上传文件不能为空");
                        }
                    }catch (Exception e){
                        out.write("上传文件格式错误");
                    }


                } else {
                    out.write("你不是超级用户，不能使用该功能");
                }
            }
            else {
                //request.getRequestDispatcher("login.jsp").forward(request, response);//转发到上传资源页面，浏览器地址不变，此过程仍在request的作用范围内
                response.sendRedirect("login.jsp");
            }


        }

        //请求查看个人主页的文章
        if("/personarticle".equals(action)) {
            JsonConversion jsonConversion=new JsonConversion();//调用json转换功能类
            System.out.println("检查。。"+request.getParameter("n"));
            FunctionalProcessing functionalProcessing=new FunctionalProcessing();
            String jsonArry=jsonConversion.json(functionalProcessing.newarticle(request.getParameter("name"),Integer.parseInt(request.getParameter("n"))));
            if(jsonArry.equals("[]")) {
                out.write("失败了！");
                // System.out.println("newarticlejsonarry1..........."+jsonArry);
            }
            else {
                out.write(jsonArry);//返回json字符串
                // System.out.println("newarticlejsonarry2..........."+jsonArry);
            }


        }

        //请求查看个人用户的粉丝，关注的人，收藏的文章
        if("/lookfans".equals(action)||"/lookguanzhu".equals(action)||"/lookcollect".equals(action)){
            JsonConversion jsonConversion = new JsonConversion();//调用json转换功能类
            List<MessageList> messageList  = null;
            if ("/lookfans".equals(action)){
                messageList = wangEditor.look(request.getParameter("name"),"查看粉丝");
            }
            if ("/lookguanzhu".equals(action)){
                messageList =  wangEditor.look(request.getParameter("name"),"查看关注");
            }
            if ("/lookcollect".equals(action)){
                messageList = wangEditor.look(request.getParameter("name"),"查看收藏");
            }
            String jsonArry = jsonConversion.json(messageList);
            if (jsonArry.equals("[]")){
                out.write("没有数据了");

            }
            else{
                out.write(jsonArry);
            }

        }


        //请求上传用户头像到服务器
        if("/uploadpicture".equals(action)) {
            if (request.getSession().getAttribute("user") != null) {

                try {
                    Part part = request.getPart("file");//获取上传的资源属性名字
                    Upload up = new Upload();//调用上传类
                    if (up.headSculpture(part, request)) {//如果返回结果为真
                        out.write("<h1 style=\"padding-top:10%;padding-left:40%; \">头像上传成功，重新登录后刷新<h1>");

                    } else {
                        out.write("<h1 style=\"padding-top:10%;padding-left:40%; \">上传错误<h1>");
                    }
                }catch (Exception e){
                    out.write("上传错误,格式错误");
                }

            }
            else {
                out.write("未登录，请求更换图片失败");
                //request.getRequestDispatcher("login.jsp").forward(request, response);//转发到上传资源页面，浏览器地址不变，此过程仍在request的作用范围内
               // response.sendRedirect("login.jsp");
            }


        }
        /*
         * 获取邮箱验证码
         * */
        if("/bindingEmail".equals(action)||"/bindingEmail2".equals(action)){
            String check ="^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)" + "?\\.)+[a-zA-Z]{2,}$";//邮箱正则表达式
            String email=request.getParameter("email");
            boolean email2=email.matches(check);
            if(email2) {
                if("/bindingEmail2".equals(action)){
                    List<User>   user=(List<User>)request.getSession().getAttribute("user");//获取保存在会话中的用户账号
                    if(user!=null&&!user.isEmpty()) {
                        String name1 = user.get(0).getName();
                        if(!wangEditor.checkemail(name1,email)){//判断是否为已绑定的邮箱
                            out.write("邮箱错误，不是绑定的邮箱");
                            return ;
                        }
                    }
                }
                SendMail sendMail = new SendMail();
                try {
                    String verifyCode = String.valueOf(new Random().nextInt(89999) + 10000);//生成5位验证码
                    request.getSession().setAttribute("EmailVerificationCode",verifyCode);
                    String s=sendMail.sendmail2(verifyCode, email, request, response);
                    out.write(s);//
                } catch (GeneralSecurityException e) {
                    // TODO 自动生成的 catch 块
                    e.printStackTrace();
                }
            }
            else{
                out.write("邮箱格式不正确");
            }

        }


        /*
        *
        * 绑定邮箱
        *
        * */
        if("/saveEmail".equals(action)){

            if (request.getSession().getAttribute("user") != null) {
                String EmailVerificationCode= (String)request.getSession().getAttribute("EmailVerificationCode");
                String code=request.getParameter("code");
                if(code!=null&&EmailVerificationCode!=null) {//判断验证码是否为空
                    if (EmailVerificationCode.equals(request.getParameter("code"))) {//判断验证码是否正确
                        if (wangEditor.bindingEmail(request.getParameter("email"), request.getParameter("name"))) {
                            out.write("绑定成功");
                            request.getSession().removeAttribute("EmailVerificationCode");
                        } else {
                            out.write("绑定失败");
                        }
                    }
                }
                else{
                    out.write("验证码错误");
                }
            }
        }

        /*
        *
        *
        *
        * 请求更改密码
        *
        *
        * */

        if("/udatepassword".equals(action)){

            if (request.getSession().getAttribute("user") != null) {
                String EmailVerificationCode = (String) request.getSession().getAttribute("EmailVerificationCode");//获取验证码
                String code = request.getParameter("code2");//获取前端验证码
                String password = request.getParameter("password");//获取前端密码
                String password2 = request.getParameter("password2");
                String email = request.getParameter("email");
                System.out.println("code" + code + "password" + password + "password2" + password2 + "email" + email);
                if (code != null && EmailVerificationCode != null && password != null && password2 != null && email != null) {//是否为空
                    if (password.equals(password2)) {
                        if (wangEditor.checkemail(request.getParameter("name"),email)){//判断邮箱是否为用户绑定的邮箱
                            if (EmailVerificationCode.equals(code)) {//判断验证码是否正确
                                if (wangEditor.udatepassword(password, request.getParameter("name"))) {
                                    out.write("修改成功");
                                    request.getSession().removeAttribute("EmailVerificationCode");
                                } else {
                                    out.write("修改失败");
                                }

                            } else {
                                out.write("验证码错误");
                            }
                        } else {
                            out.write("该邮箱不是你绑定的邮箱");
                        }
                    } else {
                        out.write("两次密码不一样");
                    }
                }
            } else {
                out.write("登录过期，请重新登录");
            }
        }

        /*
        *
        *
        * 请求刷新文章管理页面的文章
        *
        *
        * */
        if("/ArticleManager".equals(action)) {
            List <User> user=(List<User>) request.getSession().getAttribute("user");//获取当前登录用户的信息
            if(user!=null) {
                JsonConversion jsonConversion = new JsonConversion();//调用json转换功能类
                String username=user.get(0).getName();
               // FunctionalProcessing functionalProcessing = new FunctionalProcessing();
             //  String jsonArry = jsonConversion.json(functionalProcessing.newarticle(username, Integer.parseInt(request.getParameter("n"))));
                String jsonArry = jsonConversion.json( wangEditor.newarticle(username,Integer.parseInt(request.getParameter("n")),"person"));
                if (jsonArry.equals("[]")) {
                    out.write("失败了！");
                } else {
                    out.write(jsonArry);//返回json字符串

                }
            }
            else {
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        }


        /*
         *
         *
         * 请求刷新文章管理页面历史的文章
         *
         *
         * */
        if("/historyArticle".equals(action)) {
            List <User> user=(List<User>) request.getSession().getAttribute("user");//获取当前登录用户的信息
            if(user!=null) {
                JsonConversion jsonConversion = new JsonConversion();//调用json转换功能类
                String username=user.get(0).getName();
                String jsonArry = jsonConversion.json(wangEditor.historyarticle(username, Integer.parseInt(request.getParameter("n"))));
                if (jsonArry.equals("[]")) {
                    out.write("失败了！");
                } else {
                    out.write(jsonArry);//返回json字符串
                }
            }
            else {
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        }



        /*
         *
         *
         * 删除文章
         *
         *
         * */
        if("/deleteArticle".equals(action)) {

            List <User> user=(List<User>) request.getSession().getAttribute("user");//获取当前登录用户的信息
            if (user!=null) {
                try {
                    String name2 = user.get(0).getName();//用户姓名
                   if(wangEditor.deleteArticle(name2,request.getParameter("articleid"))){
                       out.write("删除成功");
                   }
                   else{
                       out.write("删除失败");
                   }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            else {
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }

        }

        /*
         *
         *
         * 删除历史
         *
         *
         * */
        if("/deletehistory".equals(action)) {

            List <User> user=(List<User>) request.getSession().getAttribute("user");//获取当前登录用户的信息
            if (user!=null) {
                try {
                    String name2 = user.get(0).getName();//用户姓名
                    if(wangEditor.deletehistory(name2)){
                        out.write("删除成功");
                    }
                    else{
                        out.write("删除失败");
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            else {
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }

        }

        /*
         *
         * 分页站点文章
         *
         *
         *
         *  */

        if ("/updateSiteArticles".equals(action)) {

            JsonConversion jsonConversion = new JsonConversion();//调用json转换功能类
            String jsonArry = jsonConversion.json(wangEditor.querySiteArticles(Integer.parseInt(request.getParameter("n"))));
            System.out.println(jsonArry);
            if (jsonArry.equals("[]")) {
                out.write("失败了！");
            } else {
                out.write(jsonArry);//返回json字符串
            }

            //request.setAttribute("SiteArticles",wangEditor.querySiteArticles(1));
            //  request.getRequestDispatcher("user/SiteArticles.jsp").forward(request, response);


        }





    }
    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @SuppressWarnings("unchecked")
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 设置响应内容类型

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String uri = request.getRequestURI();//获取页面请求路径
        String action = uri.substring(uri.lastIndexOf("/"), uri.lastIndexOf("."));
        System.out.println("get:" + action);

      /*  Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("view")) {
                    System.out.println("name为：" + cookie.getName());
                    System.out.println("cookie为：" + URLDecoder.decode(cookie.getValue(), "utf-8"));
                    request.setAttribute("view", "view");
                }

            }
        }*/


        if ("/personarticle".equals(action)) {//如果请求为查看个人文章
            System.out.println("id为[" + request.getParameter("articleid") + "]");

            List<Article> personarticle = wangEditor.query("personarticle", request.getParameter("articleid"), 1);//获取查询的文章信息
            request.setAttribute("personarticle", personarticle);//保存到会话中
            List<User> user = (List<User>) request.getSession().getAttribute("user");//获取保存在会话中的用户账号
            String name="";
            if (user != null && !user.isEmpty()) {//判断是否为游客，要是不是则添加浏览历史，24小时同一浏览器，同意用户只能加一
              name = user.get(0).getPhonenumber();
            }
                Cookie[] cookies2 = request.getCookies();
                if (cookies2 != null && cookies2.length > 0) {
                    for (Cookie cookie : cookies2) {
                        //  System.out.println(",,,,,,,,,,");
                        if (cookie.getName().equals(request.getParameter("articleid")+name)) {
                            // System.out.println("name为：" + cookie.getName());
                            //  System.out.println("articleid为：" + URLDecoder.decode(cookie.getValue(), "utf-8"));
                            request.setAttribute(request.getParameter("articleid") + name, request.getParameter("articleid") + name);
                        }


                    }

                }

                if (request.getAttribute(request.getParameter("articleid") + name) == null) {

                    Cookie articleview = new Cookie(request.getParameter("articleid") + name, URLEncoder.encode(request.getParameter("articleid") + name, "utf-8")); // 中文转码

                    //  Cookie 设置过期日期为 12小时后
                    articleview.setMaxAge(60 * 60 * 12);

                    response.addCookie(articleview);
                    System.out.println("文章" + request.getParameter("articleid") + "不存在");
                    FunctionalProcessing functionalProcessing = new FunctionalProcessing();
                    functionalProcessing.addarticleviews(request.getParameter("articleid"), (List<User>) request.getSession().getAttribute("user"), personarticle);//添加文章访问数量

                }

            try {
                List<UserComment> userComments = wangEditor.coment(request.getParameter("articleid"));//获取文章评论
                request.setAttribute("userComments", userComments);//保存文章评论到会话中
                FunctionalProcessing functionalProcessing = new FunctionalProcessing();
                //request.getSession().setAttribute("usermessage",functionalProcessing.usermessage(personarticle));//保存文章作者的信息到会话中
                request.setAttribute("usermessage", functionalProcessing.usermessage(personarticle));//保存文章作者的信息到会话中
                //List <User> user=(List<User>) request.getSession().getAttribute("user");//获取当前登录用户的信息
                request.setAttribute("messagelist", functionalProcessing.messagelist(personarticle, user));//查询该文章是否被点赞，收藏
                request.setAttribute("newarticle", functionalProcessing.newarticle(request.getParameter("name"), 1));//保存查询作者的五篇最新的文章到request
                if (personarticle != null && !personarticle.isEmpty()) {

                    request.getRequestDispatcher("user/PersonArticle2.jsp").forward(request, response);//转发到个人文章页面，浏览器地址不变，此过程仍在request的作用范围内

                    System.out.println("personarticle。。。。。。。。。。。");

                }
            }catch (Exception e){
                request.getRequestDispatcher("user/error.html").forward(request, response);//转发到个人文章页面，浏览器地址不变，此过程仍在request的作用范围内

            }


        }
        if ("/quit".equals(action)) {//用户退出请求
            try {
                List<User> user = (List<User>) request.getSession().getAttribute("user");//获取保存在会话中的用户账号
                if (user != null && !user.isEmpty()) {
                    String phonenumber = user.get(0).getPhonenumber();
                    Impl impl = new Impl();
                    impl.outtime(phonenumber);//保存用户退出时的时间
                    System.out.println("用户退出了");

                }
            } catch (Exception e) {
                // TODO: handle exception
                System.out.println(e);
            }
            // request.getSession().removeAttribute("user");//移除session中的article
            //  request.getSession().removeAttribute("news");
            request.getSession().invalidate();//移除session会话
            request.getRequestDispatcher("login.jsp").forward(request, response);//重定向到登录页面
        }


/*
        if (request.getAttribute("view") == null) {

            Cookie view = new Cookie("view", URLEncoder.encode("访问", "utf-8")); // 中文转码

            //  Cookie 设置过期日期为 12小时后
            view.setMaxAge(60 * 60 * 60);
            response.addCookie(view);
            System.out.println("cookie不存在");
            FunctionalProcessing functionalProcessing = new FunctionalProcessing();
            functionalProcessing.addpageviews(request.getParameter("address"));//添加网站访问数量

        }
*/

        /*
         *
         * 搜索文章
         *
         */

        if ("/searcharticle".equals(action)) {//搜索文章
            //response.sendRedirect("login.jsp");

            FunctionalProcessing fProcessing = new FunctionalProcessing();
            request.getSession().setAttribute("searcharticle", fProcessing.searcharticle(request.getParameter("keyboard")));
            request.setAttribute("keyboard", request.getParameter("keyboard"));
            request.getRequestDispatcher("user/SearchArticlr.jsp").forward(request, response);
            System.out.println("测试成功！！" + request.getParameter("keyboard"));

        }


        /*
         *
         *查询用户（被人关注，点赞，评论）信息提示
         *
         *
         * */

        if ("/news".equals(action)) {
            System.out.println("xxxxxx.......");
            if (request.getSession().getAttribute("user") != null) {

                System.out.println("xxxxxx.......2");
                WangEditor wangEditor = new WangEditor();
                List<User> user = (List<User>) request.getSession().getAttribute("user");//获取保存在会话中的用户账号
                request.getSession().setAttribute("commentNews", wangEditor.newsList(user.get(0).getName(), "news", 1));
                request.getRequestDispatcher("News.jsp").forward(request, response);
            } else {
                response.sendRedirect("login.jsp");
                //  request.getRequestDispatcher("login.jsp").forward(request, response);
            }


        }

        /*
         * 刷新未读信息
         * */
        if (request.getSession().getAttribute("user") != null) {

            WangEditor wangEditor = new WangEditor();
            List<User> user = (List<User>) request.getSession().getAttribute("user");//获取保存在会话中的用户账号
            // String s=Integer.toString(wangEditor.news(user.get(0).getName()));//将未读信息保存到会话中
            List<String> list = wangEditor.news(user.get(0).getName());//将未读信息保存到会话中
            request.getSession().setAttribute("news", list);
        }

        //请求进入上传资源页面
        if ("/upload".equals(action) || "/downloadlist".equals(action)) {
            if (request.getSession().getAttribute("user") != null) {
                List<User> user = (List<User>) request.getSession().getAttribute("user");//获取保存在会话中的用户账号
                User user1 = user.get(0);
                if (user1.getLevel().equals("超级会员")) {
                    if ("/upload".equals(action)) {
                        request.getRequestDispatcher("user/upload.jsp").forward(request, response);//转发到上传资源页面，浏览器地址不变，此过程仍在request的作用范围内
                    }
                    if ("/downloadlist".equals(action)) {
                        WangEditor wangEditor = new WangEditor();
                        request.getSession().setAttribute("userdocument", wangEditor.download(user1.getName()));//保存用户的文件信息到会话中
                        request.getRequestDispatcher("user/download0.jsp").forward(request, response);//转发到上传资源页面，浏览器地址不变，此过程仍在request的作用范围内
                    }
                } else {
                    out.write("你不是超级用户，不能使用该功能");
                }


            } else {
                request.getRequestDispatcher("login.jsp").forward(request, response);//转发到上传资源页面，浏览器地址不变，此过程仍在request的作用范围内

            }

        }


        //请求从服务器下载资源
        if ("/download".equals(action) || "/delete".equals(action)) {

            List<User> user = (List<User>) request.getSession().getAttribute("user");//获取保存在会话中的用户账号
            if (request.getSession().getAttribute("user") != null) {
                User user1 = user.get(0);
                if (user1.getLevel().equals("超级会员")) {
                    Upload up = new Upload();
                    if ("/download".equals(action)) {
                        try {
                            if (up.downloads(request, response, request.getParameter("filename"))) {
                                out.write("下载成功");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if ("/delete".equals(action)) {
                        if (up.deleteFile(request, request.getParameter("filename"), request.getParameter("id"))) {
                            out.write("删除成功");
                        } else {
                            out.write("删除失败");
                        }

                    }

                } else {
                    out.write("你不是超级用户，不能下载文件");
                }
            } else {
                request.getRequestDispatcher("login.jsp").forward(request, response);//转发到上传资源页面，浏览器地址不变，此过程仍在request的作用范围内

            }
        }


        //请求进入个人博客页面
        if ("/personalblog".equals(action)) {

            String name = request.getParameter("name");
            try {
                request.getSession().setAttribute("person1", impl.login("个人信息", name, null));//查看博主信息
                request.getSession().setAttribute("person2", wangEditor.articleperson(name));//查看博主的粉丝，被访问量，文章数量
                request.setAttribute("person3", wangEditor.newarticle(name, 1,null));//查看个人文章的最新五篇
                List<User> user = (List<User>) request.getSession().getAttribute("user");//获取当前登录用户的信息
                if (request.getSession().getAttribute("user") != null) {
                    FunctionalProcessing functionalProcessing = new FunctionalProcessing();
                    Article article = new Article();
                    article.setName(name);
                    List<Article> personarticle = new ArrayList<Article>();
                    personarticle.add(article);
                    request.getSession().setAttribute("guanzhu", functionalProcessing.messagelist(personarticle, user));//查询博主是否已被用户关注
                }
            } catch (Exception e) {
                System.out.println(e);
            }

            request.getRequestDispatcher("user/personalhomepage.jsp").forward(request, response);//转发到上传资源页面，浏览器地址不变，此过程仍在request的作用范围内


        }

        //请求进入个人中心页面
        if ("/editcenter".equals(action)) {

            List<User> user = (List<User>) request.getSession().getAttribute("user");//获取保存在会话中的用户账号
            if (request.getSession().getAttribute("user") != null) {
                request.getRequestDispatcher("user/editcenter.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("login.jsp").forward(request, response);

            }
        }

       /*
       *
       *
       *  请求进入文章管理页面
       *
       *
       * */
        if ("/ArticleManager".equals(action)) {

            List<User> user = (List<User>) request.getSession().getAttribute("user");//获取当前登录用户的信息
            if (user != null) {
                try {
                    String name = user.get(0).getName();//用户姓名
                    request.setAttribute("articleManager", wangEditor.newarticle(name, 1,"person"));//查看个人文章的最新五篇
                } catch (Exception e) {
                    System.out.println(e);
                }
                request.getRequestDispatcher("user/ArticleManager.jsp").forward(request, response);//转发到上传资源页面，浏览器地址不变，此过程仍在request的作用范围内
            } else {
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }

        }

        /*
         *
         *
         *
         *
         *
         * 用户编辑文章，对已发布的文章进行更改,进入编辑页面
         *
         *
         *
         *
         *
         */

        if ("/articleEditor".equals(action)) {
            List<User> user = (List<User>) request.getSession().getAttribute("user");//获取当前登录用户的信息
            if (user != null) {
                System.out.println("id为[" + request.getParameter("articleid") + "]");
                List<Article> personarticle = wangEditor.query("articleEditor", request.getParameter("articleid"), 1);//获取查询的文章信息
                //  request.setAttribute("personarticle", personarticle);//保存到会话中
                request.setAttribute("updateArticle", personarticle);
                request.getRequestDispatcher("user/wangEditor.jsp").forward(request, response);


        } else {
            request.getRequestDispatcher("login.jsp").forward(request, response);

        }

    }



       /*
       *
       * 站点文章列表
       *
       *
       *
       *  */

        if ("/SiteArticles".equals(action)) {


            request.setAttribute("SiteArticles",wangEditor.querySiteArticles(1));
            request.getRequestDispatcher("user/SiteArticles.jsp").forward(request, response);


        }

        /*
         *
         * 查看站点文章
         *
         *
         *
         *  */

        if ("/lookSiteArticles".equals(action)) {

          SiteArticles siteArticles=wangEditor.querySiteArticles2(Integer.parseInt(request.getParameter("id")));
          if(siteArticles!=null) {

              Cookie[] cookies2 = request.getCookies();
              if (cookies2 != null && cookies2.length > 0) {
                  for (Cookie cookie : cookies2) {
                      //  System.out.println(",,,,,,,,,,");
                      if (cookie.getName().equals(request.getParameter("id")+"lookSiteArticles")) {
                          // System.out.println("name为：" + cookie.getName());
                          //  System.out.println("articleid为：" + URLDecoder.decode(cookie.getValue(), "utf-8"));
                          request.setAttribute(request.getParameter("id") + "lookSiteArticles", request.getParameter("id") + "lookSiteArticles");
                      }


                  }

              }

              if (request.getAttribute(request.getParameter("id") +"lookSiteArticles") == null) {

                  Cookie articleview = new Cookie(request.getParameter("id") +"lookSiteArticles", URLEncoder.encode(request.getParameter("id") + "lookSiteArticles", "utf-8")); // 中文转码

                  //  Cookie 设置过期日期为 12小时后
                  articleview.setMaxAge(60 * 60 * 12);

                  response.addCookie(articleview);
                  System.out.println("文章" + request.getParameter("id") + "不存在");
                  wangEditor.addSiteArticlesPageView(request.getParameter("id"));

              }

              request.setAttribute("lookSiteArticles", siteArticles);
              request.getRequestDispatcher("user/lookSiteArticles.jsp").forward(request, response);


          }else {
              request.getRequestDispatcher("user/error.html").forward(request, response);

          }
        }



    }

    @Override
    public void destroy() {
        jdbc.closeDB();                                        //结束时关闭数据库
        System.out.println("serlvet被销毁了！");
    }



}
