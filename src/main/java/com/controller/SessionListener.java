package com.controller;

import java.util.List;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.dao.Impl.Impl;
import com.dao.entiy.User;

/**
 * Application Lifecycle Listener implementation class SessionListener
 *
 */
@WebListener
public class SessionListener implements HttpSessionListener {

    /**
     * Default constructor.
     */
    public SessionListener() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent se)  {
        // TODO Auto-generated method stub
        System.out.println("会话被创建了");
        //	 HttpSession session=se.getSession();
        //session.setMaxInactiveInterval(520);
    }

    /**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent se)  {
        // TODO Auto-generated method stub
        try {
            List<User>   user=(List<User>)se.getSession().getAttribute("user");//获取保存在会话中的用户账号
            if(user!=null&&!user.isEmpty()){
                String phonenumber=user.get(0).getPhonenumber();
                Impl impl=new Impl();
                impl.outtime(phonenumber);//保存用户退出时的时间
                System.out.println("用户会话被销毁了");
            }else {
                System.out.println("会话被销毁了");
            }

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }


    }

}
